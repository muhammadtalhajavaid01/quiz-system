const path = require("path");
const express = require("express"); // Express for routing etc manage
const compression = require("compression"); // Compression http requests
const next = require("next"); // Adding next
const helmet = require("helmet"); // To security etc
const routes = require("../routes"); // Importing all routes here
const port = 3101; // Assigning Port here
const dev = false; // To active in Development Environment
const app = next({ dev });

const handler = routes.getRequestHandler(app);

app.prepare().then(() => {
  const server = express();

  server.use(helmet());
  server.use(compression());

  const staticPath = path.join(__dirname, "../static");
  
  // Starting using static forlder for static contents
  server.use(
    "/static",
    express.static(staticPath, {
      maxAge: "30d",
      immutable: true
    })
  );

  // Server manage get requests that are hit in browser.
  server.get("*", (req, res) => {
    return handler(req, res);
  });

  startServer();

  // Start server method
  function startServer() {
    server.listen(port, () => {
      console.log(`> Ready on http://localhost:${port}`);
    });
  }
});

const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes()

const APP_ROUTES = [{
  page: 'index',
  pattern: '/'
}, {
  page: 'detail',
  pattern: '/detail'
},
 {
  page: 'listing',
  pattern: '/listing'
},
 {
  page: 'register',
  pattern: '/register'
}, {
  page: 'dashboard',
  pattern: '/dashboard'
}, {
  page: 'profile',
  pattern: '/profile'
}, {
  page: 'home',
  pattern: '/home'
}]

APP_ROUTES.forEach(route => routes.add(route))

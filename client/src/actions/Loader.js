// Exporting Actions Cases
export const LOADING = Symbol("LOADING");
export const SUB_LOADING = Symbol("SUB_LOADING");

// Loading Status Change Action
function loadingStatus(data) {
  return dispatch => {
    return dispatch(loadingStatusToRepos(LOADING, data));
  };
}

// Sub Loading Status Action
function subLoadingStatus(data) {
  return dispatch => {
    return dispatch(subLoadingStatusToRepos(SUB_LOADING, data));
  };
}

// Send data to Redux State
function loadingStatusToRepos(type, payload) {
  return {
    type: type,
    payload
  };
}

function subLoadingStatusToRepos(type, payload) {
  return {
    type: type,
    payload
  };
}

export { loadingStatus, subLoadingStatus };

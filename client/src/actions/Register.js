import APICall from "libs/APICall";
import Router from "next/router";

// Exporting Actions Cases
export const REGISTER = Symbol("REGISTER");
export const NOT_REGISTER = Symbol("NOT_REGISTER");
export const CHANGE_REGISTER_STATUS = Symbol("CHANGE_REGISTER_STATUS");


// Register Action
function register(data) {
  return dispatch => {
    return APICall.post("users/signup/", data).then(res => {
      if (res.status == 200 || res.status == 201) {
        if (res.data.status) {
          swal("Register!", res.data.message, "success").then(() => {
            Router.push({
              pathname: "/login"
            });
          });
        } else {
          swal("Register!", res.data.message, "warning");
        }
      } else {
        swal("Register!", res.response.data, "warning");
      }
    });
  };
}

// Change Register Action
function setRegisterStatus(data) {
  return dispatch => {
    return dispatch(registerStatusToRepos(CHANGE_REGISTER_STATUS, data));
  };
}

function registerStatusToRepos(type, payload) {
  return {
    type: type,
    payload
  };
}

export { register, setRegisterStatus };

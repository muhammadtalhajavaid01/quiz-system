import APICall from "libs/APICall";
import swal from 'sweetalert';

// Exporting Actions Cases
export const START_THREAD = Symbol("START_THREAD");
export const GET_THREADS = Symbol("GET_THREADS");
export const ADD_REPLY = Symbol("ADD_REPLY");
export const GET_REPLIES = Symbol("GET_REPLIES");

// Start New Thread Action 
function startThread(data) {
  return dispatch => {
    return APICall.post("thread/add/", data).then(res => {
      if (res.status == 200 || res.status == 201) {
        if (res.data.status) {
          window.location.reload();
        } else {
          swal("Thread!", res.data.message, "warning");
        }   
      }
    });
  };
}

// Get Threads List Action
function getThreads(data) {
  return dispatch => {
    return APICall.post("thread/get/", data).then(res => {
      if (res.status == 200 || res.status == 201) {
        dispatch(dataToRepos(GET_THREADS, res.data.threads));
      }
    });
  };
}

// Add Reply to Specific Thread
function addReply(data) {
  return dispatch => {
    return APICall.post("thread/addreply/", data).then(res => {
      if (res.status == 200 || res.status == 201) {
      }
    });
  };
}

// Get Replies Action
function getReplies(data) {
  return dispatch => {
    return APICall.post("thread/getreplies/", data).then(res => {
      if (res.status == 200 || res.status == 201) {
        dispatch(dataToRepos(GET_REPLIES, res.data.list));
      }
    });
  };
}


function dataToRepos(type, payload) {
  return {
    type: type,
    payload
  };
}

export { startThread, getThreads, addReply, getReplies };
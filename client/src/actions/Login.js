import APICall from "libs/APICall";
import Router from "next/router";
import swal from 'sweetalert';

// Exporting Actions Cases
export const LOGIN = Symbol("LOGIN");
export const NOT_LOGIN = Symbol("NOT_LOGIN");
export const START_LOADING = Symbol("START_LOADING");
export const CHANGE_LOGIN_STATUS = Symbol("CHANGE_LOGIN_STATUS");


// Login Action
function login(data) {
  return dispatch => {
    return APICall.post("users/login/", data).then(res => {
      if (res.status == 200 || res.status == 201) {
        if (res.data.status) {
          localStorage.setItem('Quiz-key', true);
          localStorage.setItem('UserName', res.data.userName);
          Router.push({
            pathname: "/home"
          });
  
        } else {
          swal("Login!", res.data.message, "warning");
        }
        
        dispatch(loginToRepos(LOGIN, {"status": true, "message": res.data.message}));
      } else {
        dispatch(loginToRepos(NOT_LOGIN, {"status": false, "message": res.data.message}));
      }
    });
  };
}

// Start Loading Action
function startLoading(data) {
  return dispatch => {
    return dispatch(startLoadingToRepos(START_LOADING, data));
  };
}

// Set Loading Status Action
function setLoginStatus(data) {
  return dispatch => {
    return dispatch(loginStatusToRepos(CHANGE_LOGIN_STATUS, data));
  };
}

// Send data to Redux State
function loginStatusToRepos(type, payload) {
  return {
    type: type,
    payload
  };
}

// Send data to Redux State
function loginToRepos(type, payload) {
  return {
    type: type,
    payload
  };
}

function startLoadingToRepos(type, payload) {
  return {
    type: type,
    payload
  };
}

export { login, startLoading, setLoginStatus };
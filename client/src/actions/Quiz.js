import APICall from "libs/APICall";

// Exporting Actions Cases
export const ADD_QUIZ_MARKS = Symbol("ADD_QUIZ_MARKS");
export const GET_QUIZ_MARKS = Symbol("GET_QUIZ_MARKS");

// Add Marks for Quiz Action
function addQuizMarks(data) {
  return dispatch => {
    return APICall.post("quiz/add/", data).then(res => {
      if (res.status == 200 || res.status == 201) {
      }
    });
  };
}

// Get Quiz Marks Action
function getQuizMarks(data) {
  return dispatch => {
    return APICall.post("quiz/get/", data).then(res => {
      if (res.status == 200 || res.status == 201) {
        dispatch(dataToRepos(GET_QUIZ_MARKS, res.data.marks));
      }
    });
  };
}

function dataToRepos(type, payload) {
  return {
    type: type,
    payload
  };
}

export { addQuizMarks, getQuizMarks };

// import Immutable from 'immutable'
import * as ActionType from "actions/Login";

// Initial State for Login Component
export const initialState = {
  data: {
    isloggedin: null,
    message: "",
    loading: false
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    // When Login Action occurs
    case ActionType.LOGIN:
      return {
        ...state,
        data: {
          isloggedin: action.payload.status,
          message: action.payload.message,
          loading: false
        }
      };

    // When Login doesn't works
    case ActionType.NOT_LOGIN:
      return {
        ...state,
        data: {
          isloggedin: action.payload.status,
          message: action.payload.message,
          loading: false
        }
      };

    // Change Login Status
    case ActionType.CHANGE_LOGIN_STATUS:
      return {
        ...state,
        data: {
          isloggedin: action.payload
        }
      };

    //  Start Login with respect to Login
    case ActionType.START_LOADING:
      return {
        ...state,
        data: {
          loading: action.payload
        }
      };
    default:
      return state;
  }
}

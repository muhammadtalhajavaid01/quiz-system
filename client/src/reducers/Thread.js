// import Immutable from 'immutable'
import * as ActionType from 'actions/Thread'

// Initial State for Thread
export const initialState = {
  data: {
    threads: [],
    list: []
  }
}

export default function (state = initialState, action) {
  switch (action.type) {

    // Update list of threads here
    case ActionType.GET_THREADS:
      return Object.assign({}, state, {
        data: Object.assign({}, state.data, {
          threads: action.payload
        })
      });

    // Update list of replies
    case ActionType.GET_REPLIES:
      return Object.assign({}, state, {
        data: Object.assign({}, state.data, {
          list: action.payload
        })
      });
    default:
      return state
  }
}
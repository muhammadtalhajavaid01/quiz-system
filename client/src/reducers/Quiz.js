// import Immutable from 'immutable'
import * as ActionType from 'actions/Quiz'

// Initial Marks state in Quiz Section
export const initialState = {
  data: {
    marks: null
  }
}

export default function (state = initialState, action) {
  switch (action.type) {

    // Get Quiz Marks and update here.
    case ActionType.GET_QUIZ_MARKS:
      return Object.assign({}, state, {
        data: Object.assign({}, state.data, {
          marks: action.payload
        })
      });
    default:
      return state
  }
}
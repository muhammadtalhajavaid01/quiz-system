// import Immutable from 'immutable'
import * as ActionType from 'actions/Register'

// Initial State for Register Component.
export const initialState = {
  data: {
    isRegistered: null,
    message: ''
  }
}

export default function (state = initialState, action) {
  switch (action.type) {

    // When registeration performs do this
    case ActionType.REGISTER:
       return {
        ...state,
        data: {
          isregistered: action.payload.status,
          message: action.payload.message
        }
      };

      // When unable to register
      case ActionType.NOT_REGISTER:
       return {
        ...state,
        data: {
          isregistered: action.payload.status,
          message: action.payload.message
        }
      };

      // Change Registration status here
      case ActionType.CHANGE_REGISTER_STATUS:
       return {
        ...state,
        data: {
          isregistered: action.payload
        }
      };
      
    default:
      return state
  }
}
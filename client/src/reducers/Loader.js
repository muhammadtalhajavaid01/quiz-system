// import Immutable from 'immutable'
import * as ActionType from 'actions/Loader'

// initial State of the loader
export const initialState = {
  data: {
    loading: true,
    sub_loading: false
  }
}

// Switch cases for Loader Component. 
export default function (state = initialState, action) {
  switch (action.type) {
    case ActionType.LOADING:
       return {
        ...state,
        data: {
            loading: action.payload
        }
      };
    
      case ActionType.SUB_LOADING:
       return {
        ...state,
        data: {
          sub_loading: action.payload
        }
      };

    default:
      return state
  }
}
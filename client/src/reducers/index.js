// This file is to combine all reducers

import { combineReducers } from 'redux'

import Register from './Register'
import Login from './Login'
import Loader from './Loader'
import Thread from './Thread';
import Quiz from './Quiz';

export default combineReducers({
  Login,
  Loader,
  Register,
  Thread,
  Quiz
})

import React from "react";
import Link from "next/link";
import MediaQuery from "react-responsive";
import styleJSX from "./Style.js";
import Router from "next/router";

class Header extends React.Component {
  state = {
    login: false
  };

  componentDidMount = () => {

    // Check is user login to show User Account dropdown in header
    const key = localStorage.getItem("Quiz-key");
    if (key) {
      this.setState({ login: true });
    } else {
      this.setState({ login: false });
    }

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains("show")) {
        openDropdown.classList.remove("show");
      }
    }
  };

  // Header dropdown management
  myFunction = () => {
    document.getElementById("myDropdown").classList.toggle("show");
  };
  render() {
    const { title } = this.props;
    return (
      <section>
        {/* Manage Application through Media  Query */}
        <MediaQuery minDeviceWidth={700}>
          <section className="gauto-header-top-area">
            <div className="container">
              <div className="row">
                <div className="col-md-6 col-sm-6 col-xs-6">
                  <div className="header-top-left">
                    <p>
                      <i className="fa fa-globe"></i> {title}
                    </p>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="header-top-right">
                    {this.state.login ? (
                      <div
                        className="dropdown"
                        onClick={() => {
                          // Clicking on Dropdown will open the Logout option for us
                          this.myFunction();
                        }}
                      >
                        <i className="dropbtn fa fa-user"></i>
                        &nbsp;
                        <span>My Account</span>&nbsp;
                        <i className="fa fa-caret-down"></i>

                        {/* If logout clicked remove user session and move to login page */}
                        <div id="myDropdown" className="dropdown-content">
                          <a
                            className="head-logout"
                            onClick={() => {
                              localStorage.removeItem("Quiz-key");
                              Router.push({
                                pathname: "/login"
                              });
                            }}
                          >
                            Logout
                          </a>
                        </div>
                      </div>
                    ) : (
                      <div>
                        <Link href="/login">
                          <a className="text-non-login">
                            <i className="fa fa-key"></i>&nbsp; login
                          </a>
                        </Link>
                        &nbsp;&nbsp;&nbsp;
                        <Link href="/register">
                          <a className="text-non-login">
                            <i className="fa fa-user"></i>&nbsp; register
                          </a>
                        </Link>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </section>
        </MediaQuery>
        
        {/* CSS added for this Header component  */}
        <style jsx>{styleJSX}</style>
      </section>
    );
  }
}

export default Header;

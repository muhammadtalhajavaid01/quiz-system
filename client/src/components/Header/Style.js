
import css from "styled-jsx/css";

export default css`

.head-logout {
  float: left;
}
.text-non-login {
  font-size: 16px;
  font-weight: 400;
  color: white;
}
.dropbtn {
  background-color: black;
  color: white;
  padding: 5px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropbtn:hover,
.dropbtn:focus {
  background-color: black;
}

.dropdown {
  position: relative;
  display: inline-block;
  cursor: pointer;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 100px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 999;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  width: 100%;
  text-align: left;
}

.dropdown a:hover {
  background-color: #ddd;
}

.mar-top {
  margin: -5px;
}
.hr-mar-pad {
  padding: 0px;
  margin: 0px;
}
.header-action {
  text-align: right;
  display:none;
}

.header-action a {
  display: inline-block;
  text-align: center;
  padding: 10px 25px 10px 0px;
  background: #198955 none repeat scroll 0 0;
  color: #fff;
  text-transform: uppercase;
  font-weight: 500;
  position: relative;
  z-index: 1;
}

.header-action a:before {
  position: absolute;
  background: #198955 none repeat scroll 0 0;
  content: "";
  top: 0;
  left: -25px;
  width: 35%;
  height: 100%;
  -webkit-transform: skewX(45deg);
  transform: skewX(45deg);
  z-index: -1;
}

.header-action a i {
  margin-right: 2px;
}

.gauto-header-top-area {
  background: #020202 none repeat scroll 0 0;
  color: #eee;
  margin-bottom: 10px;
}
.header-top-left {
  position: relative;
  z-index: 1;
  padding: 10px 0;
}
.header-top-left:before {
  position: absolute;
  background: #198955 none repeat scroll 0 0;
  content: "";
  top: 0;
  right: 37%;
  width: 2030px;
  height: 120%;
  -webkit-transform: skewX(45deg);
  transform: skewX(45deg);
  z-index: -1;
}

.header-top-left p {
  position: relative;
  top: 5px;
  font-size: 14px;
  color: #ffffff;
  font-weight: 500;
  text-transform: capitalize;
  letter-spacing: 1px;
}

.header-top-left p i {
  width: 25px;
  text-align: center;
  height: 25px;
  line-height: 25px;
  background: #fff none repeat scroll 0 0;
  color: #198955;
  border-radius: 50%;
  margin: 0 2px;
}

p {
  margin: 0;
}

.header-top-right {
  text-align: right;
  padding: 10px 0;
}

.header-top-right > a {
  color: #eee;
  text-transform: capitalize;
  margin-right: 13px;
}

a {
  background-color: transparent;
}

a {
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  color: #6b739c;
}
`;
import React from "react";
import { css } from "@emotion/core";
// First way to import
import { BeatLoader } from "react-spinners";

// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

const style = { position: "fixed", top: "50%", left: "50%", transform: "translate(-50%, -50%)" };
 
class PageLoader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }
 
  render() {
    return (
      <div className="sweet-loading" style={style}>
        <BeatLoader
          css={override}
          size={20}
          color={"#198955"}
          loading={this.state.loading}
        />
      </div>
    );
  }
}

export default PageLoader;
// This JS file is for /topBar page.

// Packages Imports
import React from "react";
import Link from "next/link";

// TopBar Component Class
class TopBar extends React.Component {
  // TopBar Constructor
  constructor(props) {
    super(props);
  }

  // TopBar Render function
  render() {
    const { activePage } = this.props;
    // Returning TopBar Container here - Starting Point
    return (
      <section className="topnav">
        <Link href="/home">
          <a className={activePage == "home" ? "active" : ""}>HomePage</a>
        </Link>

        <Link href="/social">
          <a className={activePage == "social" ? "active" : ""} href="#news">
            Socials
          </a>
        </Link>

        <Link href="/quiz">
          <a className={activePage == "info" ? "active" : ""} href="#contact">
            Important Info
          </a>
        </Link>

        <Link href="/community">
          <a
            className={activePage == "community" ? "active" : ""}
            href="#about"
          >
            Community
          </a>
        </Link>

        <style jsx>{`
          .topnav {
            overflow: hidden;
            background-color: black;
            margin-top: -10px;
          }

          .topnav a {
            float: left;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
          }

          .topnav a:hover {
            background-color: #ddd;
            color: black;
          }

          .topnav a.active {
            background-color: #4caf50;
            color: white;
          }
        `}</style>
      </section>
    );
  }
}

export default TopBar;

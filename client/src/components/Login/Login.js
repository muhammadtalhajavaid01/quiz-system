// Imports for Login Component
import React from "react";
import Link from "next/link";
import styleJSX from "../../components/Shared/css/style-form";
import PageLoader from "../../components/Shared/PageLoader";

class Login extends React.Component {
  
  // Initial State for Login
  state = {
    userName: "",
    password: ""
  };

  // UserName on Change Handler... dont allow numbers 
  userNameChangeHandler = event => {
    let value = event.target.value;
    value = value.replace(/[^A-Za-z]/gi, "");
    this.setState({ userName: value });
  };

  // Password Change Handler
  passwordChangeHandler = event => {
    this.setState({ password: event.target.value });
  };

  render() {
    return (
      <section>

        {/* Checking is loading true or not */}
        {this.props.loading ? <PageLoader /> : <div></div>}
        <div className={"page-hgt " + (this.props.loading ? "hide-login" : "")}>
          <section className="gauto-login-area section_70">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="login-box">
                    <div className="login-page-heading">
                      <i className="fa fa-key"></i>
                      <h3>sign in</h3>
                      <hr />
                    </div>
                    <form
                      onSubmit={(e) => {
                        this.props.loginHandler(
                          e,
                          this.state.userName,
                          this.state.password
                        );
                      }}
                    >
                      <div className="account-form-group">
                        <input
                          type="text"
                          placeholder="Username"
                          name="username"
                          value={this.state.userName}
                          required
                          onChange={this.userNameChangeHandler}
                        />
                        <i className="fa fa-user"></i>
                      </div>
                      <div className="account-form-group">
                        <input
                          type="password"
                          placeholder="Password"
                          name="password"
                          value={this.state.password}
                          required
                          onChange={this.passwordChangeHandler}
                        />
                        <i className="fa fa-lock"></i>
                      </div>

                      <br />
                      <p>
                        <button className="gauto-theme-btn" type="submit">
                          Continue
                        </button>
                      </p>
                    </form>
                    <div className="remember-row"></div>

                    <div className="login-sign-up">
                      <div className="form-group fs12 mb0 clearfix">
                        <Link href="/register" prefetchProps={true}>
                          <a href="/signup" className="pull-right">
                            Sign up here
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          {/* Adding CSS here */}
          <style jsx>{styleJSX}</style>
        </div>
      </section>
    );
  }
}

export default Login;

// This JS file is for /social page - Component.

// Packages Imports
import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

// Social Component Class
class Social extends React.Component {
  render() {
    // Getting Events through props from Social Container Component.
    const { events } = this.props;
    return (
      <div className="container">
        <div className="row">
          <br />
          <br />

          {/* This section is to show small Social Page Introduction */}
          <Card>
            <CardContent>
              <div className="text-just">
                <div className="fa fa-rss icon" />
                <div className="paper-content-text">
                  <p>To make all our wonderful staff feel welcome and involved within our family, we hold regular social events. On this page you can find information on upcoming socials, which is a great way to unwind and catch up with fellow colleagues. All socials are paid for by the company, and if you are interested in creating a social event please post on the community thread, if enough people respond to it, our system administrator will take it into consideration.</p>
                  <br/><b> -- Notice -- </b><p>Due to COVID-19 all physical social events have been postponed until further notice, we wish all our employees good health during these testing times, and hope they all take the utmost care, to learn more information about COVID-19 please head over to the important information page.</p>
                </div>
              </div>
            </CardContent>
          </Card>
          <br />
          <br />

          {/* This section is the table view to show social Events along with Images */}
          <table>
            <tr>
              <th>Social Event</th>
              <th>Image</th>
            </tr>

            {/* Iteration on list of events to add rows in table */}
            {events.map(obj => {
              return (
                <tr>
                  <td>
                    <p><b>Title: </b>{obj.title}</p> <p><b>Date: </b>{obj.date}</p> <p><b>Location: </b>{obj.location}</p> <p><b>Summary: </b>{obj.summary}</p>
                  </td>
                  <td>
                    <img
                      src={obj.imgSrc}
                      height="100"
                      width="120"
                    />
                  </td>
                </tr>
              );
            })}
          </table>

          <br />
          <br />
        </div>

        <style jsx global>{`
       
          .MuiPaper-rounded {
            border-style: ridge;
            border-color: lightgray;
            border-radius: 15px !important;
            border-width: 1px;
            margin-top: 20px; 
          }
        
        @media (max-width: 768px) {
          .MuiPaper-rounded {
           }
        }
        `}</style>
        
        <style jsx>{`
          table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          th {
            font-weight: 600;
            color: black;
          }

          td,
          th {
            border: 1px solid black;
            text-align: left;
            padding: 8px;
          }

          .icon {
            font-size: 30px;
            color: #198955;
          }

          .text-just {
            text-align: center;
            padding-top: 30px;
          }
        `}</style>
      </div>
    );
  }
}

export default Social;

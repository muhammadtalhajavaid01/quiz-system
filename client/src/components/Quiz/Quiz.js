// This JS file is for /quiz page - Component.

// Packages Imports
import React from "react";
import Card from "@material-ui/core/Card";
import Link from "next/link";
import CardContent from "@material-ui/core/CardContent";

// Quiz Component Class
class Quiz extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <br />
          <br />
          <Card>
            <CardContent>
              <div className="text-just">
                <div className="fa fa-star icon" />
                <div className="paper-content-text">

                  {/* Displaying Quiz Marks on UI */}
                  {(this.props.marks && this.props.marks.length > 0) ? (<p>Your Quiz Score: {this.props.marks[0].marks} / 6</p>) : <p>You have not performed Quiz yet</p> }
                  
                </div>
              </div>
            </CardContent>
          </Card>
          <br /><br />
          <Card>
            <CardContent>
              <div className="text-just">
                <div className="fa fa-rss icon" />
                <div className="paper-content-text">
                  <p>Our Health Clinic provides emergency services as well as regular appointments for clients, it is important to know how COVID-19 will be affecting the business. In this section there will be new rules and regulations mentioned that the business will implement due to COVID, as well as helpful information that we urge all our employees to know. To make learning more fun, we have added a quiz to this information, test your knowledge and compete against your colleagues!</p>
                  <br/>
                  <h3>-- Changes to the business --</h3>
                  <p>Due to COVID 19, business has been affected dramatically, we cannot offer regular appointments for our clients anymore, we will only offer emergency services to all clients, as we are a private clinic we have a limited client list, however due to the circumstances, we are temporarily opening our doors to the wider general public. Helplines will be open Monday – Friday 10am – 6pm. Walk ins are not permitted; all patients will first be assessed over the phone before being permitted any appointments. Business hours will also be affected, as our services are needed now more than ever, we have extended business hours so that the clinic will be open Monday – Saturday from 10am – 6pm.</p>
                  <br/>
                  <p>We want all our staff to be safe during these trying times, as we are key workers and work within healthcare, we are statistically of those who are under more threat to be in contact with the virus then the general public. We also understand that if staff are not well rested, they will not be able to assist to their best. For this reason, we are rolling on a 4 on 4 off day rota, with Sundays being off for all employees.</p>
                  <br/>
                  <p>COVID – 19 is a pandemic that is believed to originate from China, it is an infectious disease that attacks the respiratory systems and causes pneumonia like symptoms. The disease is airborne which means that being in the same space as one who is affected may put one at risk. Those affected by the virus do not always show the symptoms which can mean that people may not even know they are infected and can carry on infecting others, for this reason it is best to take precautions.</p>
                  <br/>
                  <p>Those that are likely to develop serious illness from the virus are the old and vulnerable. This does not go to say that healthy or young people cannot catch it. Coronavirus does not discriminate against its victims. The symptoms that one may show if affected are high temperature as well as episodes of continuous coughing.</p>
                  <br/>
                  <p>The UK have implemented lockdown, to enforce social distancing and to avoid mass breakouts of the virus. These measures have greatly slowed what could have been a disaster. Lockdown ensures that people are not allowed to come out without reason, people are permitted to leave their houses only for essential reasons such as work, shopping or their daily exercise. People that do not live in the same household need to maintain 2 meters away from each other when they are outside, and it is advised that the public wear protective gear such as gloves and masks. Action will be taken on groups that gather outside without following lockdown rules. Once people enter their homes again, they are advised to use hand sanitiser and wash their hands for a minimum of 20 seconds.</p>
                  <br/>
                  <p>For those who develop symptoms, they should do self-isolation and quarantine themselves for 14 days to see if the symptoms persist, and it is important to get tested after this 14-day process to see whether one may be infected or not. During quarantine a person should not make any contact with any other person, or they will be at risk if they in fact do have the disease. For any of our employees that may develop symptoms, please take these actions and inform us of it so that we may be able to assist you from our side, you will not be expected to come into work and you will be eligible for sick pay. Rest well knowing that we will fully support all our employees during these tough times.</p>
                </div>
              </div>
            </CardContent>
          </Card>
          <br /><br />

          <div className="row">
            <div className="col-md-5"></div>
            <div className="col-md-2">
              
              {/* Clicking here will lead to take quiz page */}
              <Link href="/quizdetail">
                <button className="gauto-theme-btn">Take Quiz !</button>
              </Link>
            </div>
            <div className="col-md-5"></div>
          </div>
          <br /><br />
        </div>

        <style jsx global>{`
          .MuiPaper-rounded {
            border-style: ridge;
            border-color: lightgray;
            border-radius: 15px !important;
            border-width: 1px;
            margin-top: 20px;
          }
          .icon {
            font-size: 30px;
            color: #198955;
          }
          .text-just {
            text-align: center;
            padding-top: 30px;
          }
        `}</style>
      </div>
    );
  }
}

export default Quiz;

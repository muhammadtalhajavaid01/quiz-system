// This JS file is for /quizdetail page - Component.

// Packages Imports
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

// This component is to show Question on the page along with its options
class Question extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    e.preventDefault();
    this.props.onChoiceChange(e.target.value);
  }
  render() {
    const question = this.props.question;
    return (
      <div className="well">
        <h3>{question.text}</h3>
        <hr />
        <ul className="list-group">
          {question.choices.map(choice => {
            return (
              <li className="list-group-item" key={choice.id}>
                {choice.id}{" "}
                <input
                  type="radio"
                  onChange={this.handleChange}
                  name={question.id}
                  value={choice.id}
                />{" "}
                {choice.text}
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

// Score box in the bar
class Scorebox extends React.Component {
  render() {
    return (
      <div className="well">
        Question {this.props.current} out of {this.props.total}
        <span className="pull-right">
          <strong>Score: {this.props.score}</strong>
        </span>
      </div>
    );
  }
}

class Results extends React.Component {
  componentDidMount = () => {
    
    // When result is announced... save quiz marks
    this.props.saveQuizMarks(this.props.score);
  }
  render() {
    const score = this.props.score;
    const total = this.props.total;
    const percent = ((score / total) * 100).toFixed(2);
    var message = "You passed the quiz!";

    // Clicking here will lead to quiz page again.

    var link = (
      <div
        className="cursor-ptr"
        onClick={() => {
          window.location.reload();
        }}
      >
        Take Again
      </div>
    );

    return (
      <div className="well">
        <h4>
          You Got {score} out of {total} Correct.
        </h4>
        <hr />
        <h1>
          {percent}% - {percent > 60 ? message : link}
        </h1>
      </div>
    );
  }
}


// This is the Main Class of this JS file - Starting Point - Export as default Component
class QuizDetail extends React.Component {
  constructor(props) {
    super(props);
    
    // This is the initial State of this component. 
    this.state = {
      score: 0, // Score obtained = 0
      current: 1, // Current Question number = 1
      active: true
    };

    // Binding function with this object, so that we can access class reference in this function
    this.handleChange = this.handleChange.bind(this);
  }

  // Question Choice Change Handler
  handleChange(choice) {
    // Updating State of this component based on score and current question number
    
    this.setState({
      active : false
    }, () => {
      this.setState({
        current: this.state.current + 1,
        score: choice == this.props.questions[this.state.current - 1].correct
        ? this.state.score + 1
        : this.state.score
      }, ()=> {
        this.setState({active: true})
      })
    })
  }

  render() {
    const questions = this.props.questions;
    return (
      <div className="container">
        <div className="row">
          <Card>
            <CardContent>
              {/* This is to Show Result when Questions finished */}
              {this.state.current > questions.length && (
                <Results 
                  total={questions.length} 
                  score={this.state.score} 
                  saveQuizMarks={this.props.saveQuizMarks}  
                />
              )}
              {/* This is score box, so score real time score to the user */}
              {this.state.current <= questions.length && (
                <Scorebox
                  total={questions.length}
                  current={this.state.current}
                  score={this.state.score}
                />
              )}

              {/* This is to show Question to the user if question is still remaining */}
              {this.state.current <= questions.length && this.state.active && (
                <Question
                  question={questions[this.state.current - 1]}
                  onChoiceChange={this.handleChange}
                />
              )}
            </CardContent>
          </Card>
          <br /> <br />
        </div>
        <style jsx global>{`
          .MuiPaper-rounded {
            border-style: ridge;
            border-color: lightgray;
            border-radius: 15px !important;
            border-width: 1px;
            margin-top: 20px;
          }
        `}</style>

        <style jsx>{`
          .cursor-ptr {
            cursor: pointer;
          }
        `}</style>
      </div>
    );
  }
}

export default QuizDetail;

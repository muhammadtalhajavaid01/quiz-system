import React from "react";
import styleJSX from "./Style.js";

// Application Footer Component
class Footer extends React.Component {
  render() {
    return (
      <section>
        {/* Footer Content here */}
        <footer className="gauto-footer-area">
          <div className="footer-top-area">
            <div className="container">
              <div className="row">
                <div className="col-lg-8">
                  <div className="single-footer">
                    <p className="rollno">
                      u1733891 - All Onboard!
                    </p>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </footer>
        {/* CSS Footer here */}
        <style jsx>{styleJSX}</style>
      </section>
    );
  }
}

export default Footer;

// This JS file is for /community - thread detail page - Component.

// Packages Imports
import React from "react";

// Community Component Class
class Community extends React.Component {
  state = {
    text: ""
  };

  textChangeHandler = event => {
    let value = event.target.value;
    this.setState({ text: value });
  };
  render() {
    // Getting Events through props from Communinty Container Component.
    return (
      <div className="container min-hgt-500">
        <div className="row">
          <br />
          <br />
          <br />

          <div className="ft-sz-21">Question: {this.props.threads.map(obj => {
            if (obj.id == this.props.selectedId) {
              return obj.question;
            }  
          })}</div>
          <br />

          {/* This section is the table view to show Community Events along with Images */}
          <table>
            <tr>
              <th>Reply</th>
              <th>By</th>
              <th>Date</th>
            </tr>

            {/* Iteration on list of events to add rows in table */}
            {this.props.list.map(obj => {
              return (
                <tr>
                  <td>{obj.reply}</td>
                  <td>{obj.userName}</td>
                  <td>{obj.createdDate}</td>
                </tr>
              );
            })}
          </table>

          <br />
          <form
            onSubmit={e => {
              this.props.addReplyToThreadHandler(
                e,
                this.state.text,
                this.props.selectedId
              );
            }}
          >
            <div className="row">
              <div className="col-lg-9 col-md-9 col-sm-9 col-xs-9 pad-right">
                <input
                  type="text"
                  className="new-thread"
                  placeholder="Reply here"
                  value={this.state.text}
                  required
                  onChange={this.textChangeHandler}
                />
              </div>
              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 ">
                <button className="gauto-theme-btn hgt-42" type="submit">
                  Reply
                </button>
              </div>
            </div>
          </form>

          <br />
        </div>

        {/* CSS imports for this file */}
        <style jsx global>{`
          .MuiPaper-rounded {
            border-style: ridge;
            border-color: lightgray;
            border-radius: 15px !important;
            border-width: 1px;
            margin-top: 20px;
          }
        `}</style>

        <style jsx>{`
          .ft-sz-21 {
            font-size: 21px;
          }
          .min-hgt-500 {
            min-height: 500px;
          }
          .new-thread {
            width: 100%;
            height: 44px;
            padding: 13px;
            border-style: solid;
            border-color: black;
            border-width: 1px;
          }

          .pad-right {
            padding-right: 0px;
          }

          .pad-left {
            padding-left: 0px;
          }

          .hgt-42 {
            height: 42px;
          }

          table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          th {
            font-weight: 600;
            color: black;
          }

          td,
          th {
            border: 1px solid black;
            text-align: center;
            padding: 8px;
          }

          .icon {
            font-size: 30px;
            color: #198955;
          }

          .text-just {
            text-align: center;
            padding-top: 30px;
          }
        `}</style>
      </div>
    );
  }
}

export default Community;

// This JS file is for /community page - Component.

// Packages Imports
import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

// Community Component Class
class Community extends React.Component {
  state = {
    text: ""
  };

  // Change Text handler for starting new thread
  textChangeHandler = event => {
    let value = event.target.value;
    this.setState({ text: value });
  };
  render() {
    // Getting Events through props from Community Container Component.
    return (
      <div className="container">
        <div className="row">
          <br />
          <br />

          {/* This section is to show small Community Page Introduction */}
          <Card>
            <CardContent>
              <div className="text-just">
                <div className="fa fa-rss icon" />
                <div className="paper-content-text">
                  <p>Welcome to the community chat! Here you can post messages for all your colleagues to view and interact with. Please note that community chat is available to all our employees and is monitored regularly to ensure safety for employees. To start a new thread use</p>
                </div>
              </div>
            </CardContent>
          </Card>
          <br />
          <br />

          {/* This section is the table view to show social Events along with Images */}
          <table>
            <tr>
              <th>Thread</th>
              <th>Link</th>
            </tr>

            {/* Iteration on list of events to add rows in table */}
            {this.props.threads.map(obj => {
              return (
                <tr>
                  <td>
                    {obj.question}
                  </td>
                  <td>
                    <div className="clickhere" onClick={()=>{
                       this.props.selectNewLink(obj.id); 
                    }}>Click to Enter Thread</div>
                  </td>
                </tr>
              );
            })}
          </table>

          <br />
          <form
          //  On Form submission start thread handler
            onSubmit={(e) => {
              this.props.startThreadHandler(e, this.state.text);
            }}
          >
            <div className="row">
              <div className="col-lg-9 col-md-9 col-sm-9 col-xs-9 pad-right">
                <input
                  type="text"
                  className="new-thread"
                  placeholder="Start a new thread"
                  value={this.state.text}
                  required
                  onChange={this.textChangeHandler}
                />
              </div>
              <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 ">
                <button className="gauto-theme-btn hgt-42" type="submit">
                  Submit
                </button>
              </div>
            </div>
          </form>

          <br />
        </div>

        {/*  CSS imports for this file */}
        <style jsx global>{`
          .MuiPaper-rounded {
            border-style: ridge;
            border-color: lightgray;
            border-radius: 15px !important;
            border-width: 1px;
            margin-top: 20px;
          }
        `}</style>

        <style jsx>{`
          .clickhere {
            cursor: pointer;
          }
          .new-thread {
            width: 100%;
            height: 44px;
            padding: 13px;
            border-style: solid;
            border-color: black;
            border-width: 1px;
          }

          .pad-right {
            padding-right: 0px;
          }

          .pad-left {
            padding-left: 0px;
          }

          .hgt-42 {
            height: 42px;
          }

          table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          th {
            font-weight: 600;
            color: black;
          }

          td,
          th {
            border: 1px solid black;
            text-align: center;
            padding: 8px;
          }

          .icon {
            font-size: 30px;
            color: #198955;
          }

          .text-just {
            text-align: center;
            padding-top: 30px;
          }
        `}</style>
      </div>
    );
  }
}

export default Community;

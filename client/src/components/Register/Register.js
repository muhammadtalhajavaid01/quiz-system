import React from "react";
import Link from "next/link";
import styleJSX from "../../components/Shared/css/style-form";

class Register extends React.Component {
  
  // Initial State of the Register Component
  state = {
    userName: "",
    email: "",
    password1: "",
    password2: ""
  };

  componentDidMount = () => {
    
    // Password and Confirm Password should be same logic
    var password = document.getElementById("password"),
    confirm_password = document.getElementById("confirmpassword");

    // Validating i.e. is Password and Confirmed Password same
    function validatePassword() {
      if (password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity("");
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
  };

  // UserName adding in state
  userNameChangeHandler = event => {
    let value = event.target.value;
    value = value.replace(/[^A-Za-z]/gi, "");
    this.setState({ userName: value });
  };

  // Email onchange adding in state
  emailChangeHandler = event => {
    this.setState({ email: event.target.value });
  };

  // password onchange adding in state
  passwordChangeHandler = event => {
    this.setState({ password1: event.target.value });
  };

  // confirm password onchange adding in state
  confirmPasswordChangeHandler = event => {
    this.setState({ password2: event.target.value });
  };

  // In this render function we are just displaying all the fileds in the form which are used in registration.
  render() {
    return (
      <div>
        <section className="gauto-login-area section_70">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="login-box">
                  <div className="login-page-heading">
                    <i className="fa fa-key"></i>
                    <h3>sign Up</h3>
                    <hr />
                  </div>
                  <form
                    onSubmit={(e) => {
                      this.props.registerHandler(
                        e,
                        this.state.userName,
                        this.state.email,
                        this.state.password1
                      );
                    }}
                  >
                    <div className="account-form-group">
                      <input
                        type="text"
                        placeholder="Username"
                        name="username"
                        value={this.state.userName}
                        required
                        onChange={this.userNameChangeHandler}
                      />
                      <i className="fa fa-user"></i>
                    </div>
                    <div className="account-form-group">
                      <input
                        type="email"
                        placeholder="Email Address"
                        name="EmailAddress"
                        value={this.state.email}
                        required
                        onChange={this.emailChangeHandler}
                      />
                      <i className="fa fa-envelope-o"></i>
                    </div>

                    <div className="account-form-group">
                      <input
                      id="password"
                        type="password"
                        placeholder="Password"
                        name="password"
                        value={this.state.password1}
                        required
                        onChange={this.passwordChangeHandler}
                      />
                      <i className="fa fa-lock"></i>
                    </div>
                    <div className="account-form-group">
                      <input
                        id="confirmpassword"
                        type="password"
                        placeholder="Confirm Password"
                        name="confirmPassword"
                        value={this.state.password2}
                        required
                        onChange={this.confirmPasswordChangeHandler}
                      />
                      <i className="fa fa-lock"></i>
                    </div>
                    <div className="remember-row">
                      <p className="checkbox remember signup"></p>
                    </div>
                    <p>
                      <button className="gauto-theme-btn" type="submit">
                        Register
                      </button>
                    </p>
                  </form>
                  <div className="login-sign-up">
                    <div className="form-group fs12 mb0 clearfix">
                      <Link href="/login">
                        <a>Sign in</a>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <style jsx>{styleJSX}</style>
      </div>
    );
  }
}

export default Register;

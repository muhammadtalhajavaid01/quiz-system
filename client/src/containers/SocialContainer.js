// This JS file is for /social page - Container.

// Packages Imports
import React, { Component } from "react";
import { connect } from "react-redux"; // To connect with redux for State Management
import Header from "components/Header/Header"; // Header
import Footer from "components/Footer/Footer"; // Footer
import Social from "components/Social/Social"; // Social Component
import TopBar from 'components/shared/TopBar'; // Top Menu Bar

// Social Events List Defined here - Count = 3
const events = [{
  id: 1,
  title: 'IPG Paintballing',
  date: 'Postponed',
  location: 'Upminister',
  summary: 'Step out with your comrades in the battlefield! Come and join the team in a day full of paintballing, featuring team modes as well as free for all matches, this is one to watch out for!',
  imgSrc: 'https://images.idgesg.net/images/article/2020/03/microsoft-teams-logo-100834384-orig.jpg' 
},{
  id: 2,
  title: 'Chess Tournament',
  date: '20/06/2020',
  location: 'Online',
  summary: 'This was originally meant to be held in the clinic, a chess tournament where the winner gets a worthy prize, due to COVID-19 we do not want employees gathering in the business unless necessary, so all matches will be held online. To check the rota for games please check your emails. To enlist for this tournament please contact: DwightSchrute@email.com as he has set up the chess tournament.',
  imgSrc: 'https://images.idgesg.net/images/article/2020/03/microsoft-teams-logo-100834384-orig.jpg' 
},{
  id: 3,
  title: 'Yoga',
  date: 'Postponed',
  location: 'Bloomfield Suite',
  summary: 'Here at our health clinic, our small family is always working its hardest to be the best for our customers, at times it is important to stop and relax. Join us for a relaxing afternoon to revitalise your spirits.  With a 30-minute session of Yoga and meditation, after unwinding the afternoon will be ended with pleasant afternoon tea, accompanied with finger foods and cakes.',
  imgSrc: 'https://images.idgesg.net/images/article/2020/03/microsoft-teams-logo-100834384-orig.jpg' 
}]


// Social Container Class
class SocialContainer extends Component {
  componentDidMount = () => {
    const key = localStorage.getItem("Quiz-key");
    if (!key) {
      window.location.href = "/";
    }
  };
  render() {
    // Return Statement must contain single Element at Outer component.
    return (
      <section>
        <Header title="Socials"/>
        <TopBar activePage='social'/>
        {/* Social Component - Passing Events list in this */}
        <Social events={events}/>
        <Footer />
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export { SocialContainer };

// Connecting Action methods here.
export default connect(mapStateToProps, {})(SocialContainer);

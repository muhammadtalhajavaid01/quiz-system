// This JS file is for /community page - Container.

// Packages Imports
import React, { Component } from "react";
import { connect } from "react-redux"; // To connect with redux for State Management
import Header from "components/Header/Header"; // Header
import Footer from "components/Footer/Footer"; // Footer
import Community from "components/Community/Community"; // Community Component
import ThreadDetails from "components/Community/ThreadDetails"; // Thread Replies Component
import TopBar from "components/shared/TopBar"; // Top Menu Bar
import {
  startThread,
  getThreads,
  addReply,
  getReplies
} from "../actions/Thread"; // Redux thread actions

// Community Container Class
class CommunityContainer extends Component {
  componentDidMount = () => {
    // Check login status if true ok else move to login page.
    const key = localStorage.getItem("Quiz-key");
    if (!key) {
      window.location.href = "/";
    } else {
      // Fetch list of all thread initially
      const { getThreads } = this.props;
      getThreads();
    }
  };

  state = {
    selectedId: -1
  };

  // Selected Thread to enter
  selectNewLink = id => {
    this.setState({ selectedId: id });

    const { getReplies } = this.props;

    const data = {
      id: id
    };

    getReplies(data);
  };

  // Start new thread by writing down question and submitting.
  startThreadHandler = (e, text) => {
    e.preventDefault();
    const { startThread } = this.props;

    const data = {
      text: text
    };

    startThread(data);
  };

  // This method is to add reply against specific thread.
  addReplyToThreadHandler = (e, reply, selectedId) => {
    e.preventDefault();
    const { addReply, getReplies } = this.props;

    const data = {
      id: selectedId,
      userName: localStorage.getItem("UserName"),
      reply: reply
    };

    // Calling addReply function add reply through Server.
    addReply(data).then(() => {
      const data = {
        id: selectedId
      };
      getReplies(data);
    });
  };
  render() {
    // Return Statement must contain single Element at Outer component.
    return (
      <section>
        <Header title="Community" />
        <TopBar activePage="community" />
        {/* If No thread is selected show all threads on community page */}
        {/* And if thread is selected show replies against that thread */}
        {this.state.selectedId == -1 ? (
          <Community
            startThreadHandler={this.startThreadHandler}
            threads={this.props.threads}
            selectNewLink={this.selectNewLink.bind(this)}
          />
        ) : (
          <ThreadDetails
            list={this.props.list}
            threads={this.props.threads}
            addReplyToThreadHandler={this.addReplyToThreadHandler}
            selectedId={this.state.selectedId}
          />
        )}

        <Footer />
      </section>
    );
  }
}

// Getting Required data from redux state ... List of threads and List of Specific thread replies.
function mapStateToProps(state) {
  return {
    threads: state.Thread.data.threads,
    list: state.Thread.data.list
  };
}

export { CommunityContainer };

// Connecting Action methods here.
export default connect(mapStateToProps, {
  startThread,
  getThreads,
  addReply,
  getReplies
})(CommunityContainer);

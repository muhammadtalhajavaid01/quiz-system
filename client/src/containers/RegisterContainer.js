import React, { Component, Fragment } from "react";

import { connect } from "react-redux";
import Header from "components/Header/Header";
import Register from "components/Register/Register";
import Footer from "components/Footer/Footer";
import { register, setRegisterStatus } from "actions/Register";
import { loadingStatus } from "actions/Loader";
import Router from "next/router";
import PageLoader from '../components/Shared/PageLoader';

class RegisterContainer extends Component {
  componentWillMount = () => {
    // Loading Status Check
    Router.events.on("routeChangeStart", () => {
      const {loadingStatus} = this.props;
      loadingStatus(true);
    });
  }

  componentDidMount = () => {
    // On Mount Complete remove loading
    const { loadingStatus } = this.props;
    loadingStatus(false);
  }

  // Registration Handler
  registerHandler = (e, userName, email, password) => {
    e.preventDefault();
    const { register, loadingStatus } = this.props;

    // Login Status true on registration check
    loadingStatus(true);

    register({
      userName,
      email,
      password
    }).then(() => {

      // Loading status false on registration complete
      loadingStatus(false);
    });
  };

  render() {
    return (
      <div>
        {/* Checking loading Status here */}
        {this.props.loading ? <PageLoader /> : (
        <section>
          <Header title="Sign up"/>
          {/* Register Component */}
          <Register
            registerHandler={this.registerHandler}
          />
          <Footer />
        </section>
        )}
      </div>
    );
  }
}

// Getting Required data from redux state
function mapStateToProps(state) {
  return {
    isRegistered: state.Register.data.isregistered,
    message: state.Register.data.message,
    loading: state.Loader.data.loading
  };
}

export { RegisterContainer };
// Connecting Action methods here.
export default connect(mapStateToProps, {
  register,
  loadingStatus,
  setRegisterStatus
})(RegisterContainer);

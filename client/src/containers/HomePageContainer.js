// This JS file is for /home page - Container.

// Packages Imports
import React, { Component } from "react";
import { connect } from "react-redux"; // To connect with redux for State Management
import Header from "components/Header/Header"; // Header
import Footer from "components/Footer/Footer"; // Footer
import Home from "components/Home/Home"; // Home Component
import TopBar from "components/shared/TopBar";

// Home Page Container Class
class HomePageContainer extends Component {
  componentDidMount = () => {
    // Check login status if true ok else move to login page.
    const key = localStorage.getItem("Quiz-key");
    if (!key) {
      window.location.href = "/";
    }
  };
  render() {
    // Return Statement must contain single Element at Outer component.
    return (
      <section>
        <Header title="Homepage" />
        <TopBar activePage="home" />
        {/* Home Component */}
        <Home /> 
        <Footer />
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export { HomePageContainer };
export default connect(mapStateToProps, {})(HomePageContainer);

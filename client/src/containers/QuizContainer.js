// This JS file is for /quiz page - Container.

// Packages Imports
import React, { Component } from "react";
import { connect } from "react-redux";
import Header from "components/Header/Header"; // Header
import Quiz from "components/Quiz/Quiz"; // Quiz Component
import Footer from "components/Footer/Footer"; // Header
import TopBar from 'components/shared/TopBar'; // Top Menu Bar

import { addQuizMarks, getQuizMarks } from "../actions/Quiz";

// Quiz Container Class
class QuizContainer extends Component {
  componentDidMount = () => {
    // Check login status if true ok else move to login page.
    const key = localStorage.getItem("Quiz-key");
    if (!key) {
      window.location.href = "/";
    } else {
      const { getQuizMarks } = this.props;
      
      var data = {
        userName: localStorage.getItem('UserName')
      }
      // Intially Get User QUiz Marks if exists
      getQuizMarks(data);
    }
  };
  render() {
    // Return Statement must contain single Element at Outer component.
    return (
      <section>
        <Header title="Important Information"/>
        <TopBar activePage="info" />

        {/* Passing Quiz Marks in Quiz Component to show on UI */}
        <Quiz marks={this.props.marks}/>
        <Footer />
      </section>
    );
  }
}

// Getting Required data from redux state
function mapStateToProps(state) {
  return {
    marks: state.Quiz.data.marks
  };
}

export { QuizContainer };

// Connecting Action methods here.
export default connect(mapStateToProps, {
  addQuizMarks,
  getQuizMarks
})(QuizContainer);

import React, { Component, Fragment } from "react";

import { connect } from "react-redux";
import Header from "components/Header/Header";
import Login from "components/Login/Login";
import Footer from "components/Footer/Footer";
import { loadingStatus, subLoadingStatus } from "actions/Loader";
import { login, setLoginStatus } from "actions/Login";
import PageLoader from "../components/Shared/PageLoader";
import Router from "next/router";

class LoginContainer extends Component {
  componentDidMount = () => {
    // Check login status if true ok else move to login page.
    const key = localStorage.getItem("Quiz-key");
    if (key) {
      Router.push({
        pathname: "/home"
      });
    } else {
      // Remove loading bar on mounting complete
      const { loadingStatus } = this.props;
      loadingStatus(false);
    }
  };

  // On login click handle it by call server login API
  loginHandler = (e, userName, password) => {
    e.preventDefault();
    const { login, subLoadingStatus } = this.props;
    subLoadingStatus(true);

    login({
      userName,
      password
    });
  };

  render() {
    return (
      <div>
        {/* Login Page Loader during Mount */}
        {this.props.loading ? (
          <PageLoader />
        ) : (
          <section>
            <Header title="Login" />
            {/* Login Component passing Handler function */}
            <Login
              loginHandler={this.loginHandler}
              loading={this.props.sub_loading}
            />
            <Footer />
          </section>
        )}
      </div>
    );
  }
}

// Getting Required data from redux state
function mapStateToProps(state) {
  return {
    loading: state.Loader.data.loading
  };
}

export { LoginContainer };

// Connecting Action methods here.
export default connect(mapStateToProps, {
  loadingStatus,
  subLoadingStatus,
  login,
  setLoginStatus
})(LoginContainer);

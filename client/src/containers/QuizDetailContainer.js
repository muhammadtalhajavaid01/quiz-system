// This JS file is for /quizdetail page - Container.

// Packages Imports
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Header from "components/Header/Header"; // Header
import QuizDetail from "components/Quiz/QuizDetail"; // Quiz Questions Component
import Footer from "components/Footer/Footer"; // Footer
import TopBar from "components/shared/TopBar"; // Top Menu Bar

import { addQuizMarks, getQuizMarks } from "../actions/Quiz";

// Quiz Questions List
var QUESTIONS = [
  {
    id: 1,
    text: "1)	Due to COVID, what work rota are we implementing?",
    choices: [
      {
        id: "a",
        text: "4 day on 3 day off"
      },
      {
        id: "b",
        text: "4 day on 4 day off"
      },
      {
        id: "c",
        text: "5 day on 2 day off"
      },
      {
        id: "d",
        text: "5 day on 3 day off"
      }
    ],
    correct: "b"
  },
  {
    id: 2,
    text: "2)	What day is the clinic closed during the week?",
    choices: [
      {
        id: "a",
        text: "Monday"
      },
      {
        id: "b",
        text: "Thursday"
      },
      {
        id: "c",
        text: "Saturday"
      },
      {
        id: "d",
        text: "Sunday"
      }
    ],
    correct: "d"
  },
  {
    id: 3,
    text: "3)	What is a valid reason for leaving the house during lockdown?",
    choices: [
      {
        id: "a",
        text: "Daily exercise"
      },
      {
        id: "b",
        text: "Essentials shopping"
      },
      {
        id: "c",
        text: "Both of the above"
      },
      {
        id: "d",
        text: "None of the above, during lockdown one is prohibited to leave the house"
      }
    ],
    correct: "c"
  },
  {
    id: 4,
    text: "4)	What is a valid reason for leaving the house during 14 day quarantine?",
    choices: [
      {
        id: "a",
        text: "Work"
      },
      {
        id: "b",
        text: "Essentials shopping"
      },
      {
        id: "c",
        text: "Daily exercise"
      },
      {
        id: "d",
        text: "None of the above, during quarantine one is prohibited to leave the house"
      }
    ],
    correct: "d"
  },
  {
    id: 5,
    text: "5)	What are the symptoms of coronavirus?",
    choices: [
      {
        id: "a",
        text: "Laboured breathing and Dilated pupils"
      },
      {
        id: "b",
        text: "Nausea and Continuous cough"
      },
      {
        id: "c",
        text: "Continuous cough and High temperature"
      },
      {
        id: "d",
        text: "Hallucinations and Headaches"
      }
    ],
    correct: "c"
  },
  {
    id: 6,
    text: "6)	What is the minimum duration we are advised to wash our hands for after re-entering our homes?",
    choices: [
      {
        id: "a",
        text: "15 seconds"
      },
      {
        id: "b",
        text: "20 seconds"
      },
      {
        id: "c",
        text: "24 seconds"
      },
      {
        id: "d",
        text: "30 seconds"
      }
    ],
    correct: "b"
  }
];

// Quiz Questions Container - Passing Questions to Quiz Detail Component
class QuizDetailContainer extends Component {
  componentDidMount = () => {
    // Check login status if true ok else move to login page.
    const key = localStorage.getItem("Quiz-key");
    if (!key) {
      window.location.href = "/";
    }
  };

  // Save Quiz Marks on Attempt by passing User Name and Marks
  saveQuizMarks = marks => {
    const { addQuizMarks } = this.props;
    var data = {
      userName: localStorage.getItem("UserName"),
      marks: marks
    };

    addQuizMarks(data);
  };

  render() {
    // Return Statement must contain single Element at Outer component.
    return (
      <section>
        {/* Header */}
        <Header title="Quiz" />

        {/* Top Menu Bar */}
        <TopBar />

        {/* Quiz Questions Component */}
        <QuizDetail questions={QUESTIONS} saveQuizMarks={this.saveQuizMarks} />

        {/* Footer */}
        <Footer />
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export { QuizDetailContainer };

// Connecting Action methods here.
export default connect(mapStateToProps, {
  addQuizMarks,
  getQuizMarks
})(QuizDetailContainer);

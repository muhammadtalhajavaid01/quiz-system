export default {
  env: process.env.NODE_ENV,
  REST_END_POINT: 'http://localhost:3600/'
}
// This file is for Axios Calls... Managing Get / Post / Put

// Getting base url from config file

// Returning all calls response back to Actions promise then

// Imports
import axios from "axios";
import config from "config";

const APICall = {
  get(path, header) {
    let configHeader = {
      headers: header
    };
    const urlPath = `${config.REST_END_POINT}${path}`;
    return axios.get(urlPath, configHeader).then(res => {
      return res;
    });
  },
  post(path, data) {
    const urlPath = `${config.REST_END_POINT}${path}`;
    return axios
      .post(urlPath, data)
      .then(res => {
        return res;
      })
      .catch(function(error) {
        return error;
      });
  },
  put(path, data, header) {
    let configHeader = {
      headers: header
    };
    const urlPath = `${config.REST_END_POINT}${path}`;
    return axios
      .put(urlPath, data, configHeader)
      .then(res => {
        return res;
      })
      .catch(function(error) {
        return error;
      });
  }
};

export default APICall;

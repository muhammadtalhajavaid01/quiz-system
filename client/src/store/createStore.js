import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware, compose } from 'redux'

import config from 'config'
import rootReducer from '../reducers'

function createMiddlewares ({ isServer }) {
  const middlewares = [
    thunkMiddleware
  ]

  if (config.env === 'development' && typeof window !== 'undefined') {
    middlewares.push(createLogger({
      level: 'info',
      collapsed: true,
      stateTransformer: (state) => {
        const newState = {}
        return newState
      }
    }))
  }

  return middlewares
}

export default (initialState = {}, context) => {
  const { isServer } = context
  const middlewares = createMiddlewares({ isServer })
 
  return createStore(
    rootReducer,
    compose(applyMiddleware(...middlewares))
  )
}

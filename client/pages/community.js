// This JS file is for /community page.

// Packages Imports
import React from "react";
import CommunityContainer from "../src/containers/CommunityContainer";

// React Component Class 
class CommunityPage extends React.Component {
  // Community Page Constructor
  constructor(props) {
    super(props);
  }

  // Community Page Render function
  render() {
    // Returning Community Page Container here - Starting Point
    return <CommunityContainer />
  }
}

export default CommunityPage;
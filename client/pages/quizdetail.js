// Quiz Details => Questions JS file

// Imports
import React from "react";
import QuizDetailContainer from "../src/containers/QuizDetailContainer";

// QuizDetails Page Component
class QuizDetailPage extends React.Component {
  constructor(props) {
    super(props);
  }

  // Render Method
  render() {
    return (
      <div>
        {/* Quiz Container  - Passing Function to save Quiz Marks*/}
        <QuizDetailContainer saveQuizMarks={this.props.saveQuizMarks} />
      </div>
    );
  }
}

export default QuizDetailPage;
// This JS file is for /quiz page.

// Packages Imports
import React from "react";
import QuizContainer from "../src/containers/QuizContainer";

// Quiz Component Class 
class QuizPage extends React.Component {

  // Quiz Constructor
  constructor(props) {
    super(props);
  }

  // Quiz Page Render function
  render() {
    // Returning Quiz Page Container here - Starting Point
    return <QuizContainer />
  }
}

export default QuizPage;
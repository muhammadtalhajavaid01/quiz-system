import Document, { Head, Main, NextScript } from "next/document";
import { ServerStyleSheet } from "styled-components";
import Helmet from "react-helmet";

export default class extends Document {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;
    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: App => props => sheet.collectStyles(<App {...props} />)
        });

      const documentProps = await Document.getInitialProps(ctx);
      return {
        ...documentProps,
        helmet: Helmet.renderStatic(),
        styles: (
          <>
            {documentProps.styles}
            {sheet.getStyleElement()}
          </>
        )
      };
    } finally {
      sheet.seal();
    }
  }

  get helmetHtmlAttrComponents() {
    return this.props.helmet.htmlAttributes.toComponent();
  }

  get helmetBodyAttrComponents() {
    return this.props.helmet.bodyAttributes.toComponent();
  }

  get helmetHeadComponents() {
    return Object.keys(this.props.helmet)
      .filter(el => el !== "htmlAttributes" && el !== "bodyAttributes")
      .map(el => this.props.helmet[el].toComponent());
  }

  get helmetJsx() {
    const title = "Quiz System";
    return (
      <Helmet>
        <title>{title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:title" content={title} />
      </Helmet>
    );
  }

  render() {
    return (
      <html {...this.helmetHtmlAttrComponents}>
        <Head>
          {this.helmetJsx}
          {this.helmetHeadComponents}
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          />
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          />
        </Head>
        <body {...this.helmetBodyAttrComponents}>
          <Main />
          <NextScript />
        </body>
        <style jsx global>
          {`  
            body {
              font-size: 14px;
              line-height: 26px;
              font-style: normal;
              color: #7c8a97;
              font-family: "Rubik", sans-serif;
              font-weight: 400;
              text-rendering: optimizelegibility;
              -moz-osx-font-smoothing: grayscale;
              -webkit-font-smoothing: antialiased;
            }
            
            html,
            body {
              height: 100%;
            }
            
            .col-pad-0 {
              padding-right: 0;
              padding-left: 0;
            }
            
            .logo-hgt {
              height: 48px;
              margin-bottom: 6px;          
            }
            
            p {
              margin: 0;
            }
            
            .no-pad-left {
              padding-left: 0;
            }
            
            .no-pad-right {
              padding-right: 0;
            }
            
            .no-pad-all {
              padding: 0;
            }
            
            .fix {
              overflow: hidden;
            }
            
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
              margin: 0;
              padding: 0;
            }
            
            a {
              -webkit-transition: all 0.4s ease 0s;
              transition: all 0.4s ease 0s;
              color: #6b739c;
            }
            
            a:focus {
              text-decoration: none;
              outline: medium none;
              color: inherit;
            }
            
            a:hover {
              text-decoration: underline;
              color: white;
              cursor: pointer;
            }
            
            input:focus,
            textarea:focus,
            button:focus,
            select:focus {
              outline: medium none;
            }
            
            :-moz-placeholder {
              color: #7c8a97;
              text-transform: capitalize;
            }
            
            ::-moz-placeholder {
              color: #7c8a97;
              text-transform: capitalize;
            }
            
            :-ms-input-placeholder {
              color: #7c8a97;
              text-transform: capitalize;
            }
            
            ::-webkit-input-placeholder {
              color: #7c8a97;
              text-transform: capitalize;
            }
            
            :-ms-select-placeholder {
              color: #7c8a97;
              text-transform: capitalize;
            }
            
            ::-webkit-select-placeholder {
              color: #7c8a97;
              text-transform: capitalize;
            }
            
            :-ms-textarea-placeholder {
              color: #7c8a97;
              text-transform: capitalize;
            }
            
            ::-webkit-textarea-placeholder {
              color: #7c8a97;
              text-transform: capitalize;
            }
            
           
            
            ul,
            ol {
              margin: 0;
              padding: 0;
            }
            
            
            .find-btn-submit {
              margin: 18px 0 !important;
            }
            
            .gauto-btn,
            button.gauto-theme-btn {
              color: #fff;
              background: transparent;
              border-width: 2px;
              border-style: solid;
              border-color: #198955;
              position: relative;
              margin: 1em;
              display: inline-block;
              padding: 8px 15px;
              -webkit-transition: all 0.3s ease-in-out;
              transition: all 0.3s ease-in-out;
              text-align: center;
              font-weight: 500;
              text-transform: uppercase;
              letter-spacing: 1px;
            }
            
            .gauto-btn:before,
            .gauto-btn:after,
            button.gauto-theme-btn:before,
            button.gauto-theme-btn:after {
              content: "";
              display: block;
              position: absolute;
              border-color: #198955;
              box-sizing: border-box;
              border-style: solid;
              width: 1em;
              height: 1em;
              -webkit-transition: all 0.3s ease-in-out;
              transition: all 0.3s ease-in-out;
            }
            
            .gauto-btn:before,
            button.gauto-theme-btn:before {
              top: -6px;
              left: -6px;
              border-width: 2px 0 0 2px;
              z-index: 5;
            }
            
            .gauto-btn:after,
            button.gauto-theme-btn:after {
              bottom: -6px;
              right: -6px;
              border-width: 0 2px 2px 0;
            }
            
            .gauto-btn:hover:before,
            .gauto-btn:hover:after,
            button.gauto-theme-btn:hover:before,
            button.gauto-theme-btn:hover:after {
              width: calc(100% + 12px);
              height: calc(100% + 12px);
              border-color: #198955;
            }
            
            .gauto-btn:hover,
            button.gauto-theme-btn:hover {
              color: #fff;
              background-color: #198955;
              border-color: #198955;
            }
            
            button.gauto-theme-btn {
              color: #198955;
              background: transparent;
              border-width: 2px;
              border-style: solid;
              border-color: #198955;
              position: relative;
              display: block;
              padding: 8px 15px;
              -webkit-transition: all 0.3s ease-in-out;
              transition: all 0.3s ease-in-out;
              text-align: center;
              font-weight: 500;
              text-transform: uppercase;
              letter-spacing: 1px;
              cursor: pointer;
              margin: 0;
              width: 100%;
            }
            
            button.gauto-theme-btn:before,
            button.gauto-theme-btn:after {
              content: "";
              display: block;
              position: absolute;
              border-color: #198955;
              box-sizing: border-box;
              border-style: solid;
              width: 1em;
              height: 1em;
              -webkit-transition: all 0.3s ease-in-out;
              transition: all 0.3s ease-in-out;
            }
            
            .gauto-btn:before,
            button.gauto-theme-btn:before {
              top: -6px;
              left: -6px;
              border-width: 2px 0 0 2px;
              z-index: 5;
            }
            
            .gauto-btn:after,
            button.gauto-theme-btn:after {
              bottom: -6px;
              right: -6px;
              border-width: 0 2px 2px 0;
            }
            
            button.gauto-theme-btn:hover:before,
            button.gauto-theme-btn:hover:after {
              width: calc(100% + 12px);
              height: calc(100% + 12px);
              border-color: #198955;
            }
            
            .gauto-btn:hover,
            button.gauto-theme-btn:hover {
              color: #fff;
              background-color: #198955;
              border-color: #198955;
            }
            
            .site-heading {
              margin-bottom: 30px;
              width: 60%;
              margin: 0 auto;
              text-align: center;
            }
            
            .site-heading h4 {
              font-size: 20px;
              color: #198955;
              margin-bottom: 0px;
              display: block;
              font-weight: 500;
              text-transform: capitalize;
              font-family: "Rubik", sans-serif;
            }
            
            .site-heading h2 {
              font-size: 40px;
              color: #001238;
              letter-spacing: 1px;
              -webkit-transition: all 0.4s ease 0s;
              transition: all 0.4s ease 0s;
              display: inline-block;
              text-transform: capitalize;
              font-family: "Rubik", sans-serif;
              font-weight: 500;
            }
            
            .section_100 {
              padding: 100px 0;
            }
            
            .section_70 {
              padding: 70px 0;
            }

            .pad-top-70 {
              padding-top: 70px;
            }

            .pad-top-50 {
              padding-top: 50px;
            }
            
            .section_50 {
              padding: 50px 0;
            }
            
            .section_15 {
              padding: 15px 0;
            }
            
            .pad-right {
              padding-right: 0;
            }
            
            .section_t_100 {
              padding-top: 100px;
            }
            
            .section_b_70 {
              padding-bottom: 70px;
            }
            
            .section_70 {
              padding: 70px 0;
            }
            
            .section_b_80 {
              padding-bottom: 80px;
            }
            
            .my-carousel {
              height: 175px;
            }
            .carousel .next,
            .carousel .prev {
              top: calc(50% - 50px) !important;
            }
            .carousel .carousel-footer {
              background-color: white !important;
            }
            
            .carousel .carousel-main {
              height: calc(100% - 50px) !important;
              background-color: white !important;
            }
            
            .car-offer .carousel-main img {
              width: 200px !important;
              height: 125px !important;
              min-width: 50%;
              min-height: 50%;
            }
            
            .slider {
              height: 600px !important;
            }
            
            /* React - Select */
            
            .css-yk16xz-control,
            .css-1pahdxg-control {
              margin: 15px 0 !important;
              width: 100%;
              border: 2px solid #f0f0ff;
              height: 45px;
              color: #111;
              -webkit-transition: all 0.4s ease 0s;
              -webkit-transition: all 0.4s ease 0s;
              transition: all 0.4s ease 0s;
              font-size: 16px;
              border-radius: 4px;
            }
            
            .css-1pahdxg-control,
            .css-1pahdxg-control:hover {
              border-color: #f0f0ff !important;
              box-shadow: 0 0 0 1px #f0f0ff;
              box-shadow: 0 0 0 1px #f0f0ff !important;
            }
            
            .css-1pahdxg-control:hover {
              border-color: #f0f0ff !important;
              box-shadow: 0 0 0 1px #f0f0ff;
            }
            
            .css-9gakcf-option {
              background-color: #198955 !important;
            }
            
            .PrivateValueLabel-label-63 {
              font-size: 16px;
            }
            .react-autosuggest__suggestions-list {
              overflow-y: auto;
            }
            
            /*================================================
            01 - IMPORTED CSS
            ==================================================*/
            
            @import url("https://fonts.googleapis.com/css?family=Poppins:500,500i,600,600i,700,700i,800,800i,900,900i&display=swap");
            @import url("https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i&display=swap");
            
            /*================================================
            02 - DEFAULT CSS
            ==================================================*/

            
            /*================================================
            03 - RESPONSIVE CSS
            ==================================================*/
            
            .gauto-responsive-menu {
              display: none;
              position: absolute;
              right: 0;
              top: 15px;
              width: 100%;
              z-index: 999999;
            }
            
            .slicknav_nav a {
              font-size: 14px;
              margin: 0;
              text-transform: capitalize;
              -webkit-transition: all 0.4s ease 0s;
              transition: all 0.4s ease 0s;
              color: #000;
            }
            
            .slicknav_menu {
              background: transparent;
            }
            
            .slicknav_menutxt {
              display: none !important;
            }
            
            .slicknav_arrow {
              float: right;
              color: #198955;
            }
            
            .slicknav_nav a:hover {
              border-radius: 0;
            }
            
            .slicknav_nav .slicknav_row:hover {
              border-radius: 0;
            }
            
            .slicknav_nav a:hover,
            .slicknav_nav .slicknav_row:hover {
              background: #198955 none repeat scroll 0 0;
              color: #fff;
            }
            
            .slicknav_nav a:hover .slicknav_arrow {
              color: #fff;
            }
            
            .slicknav_nav a:hover a {
              color: #fff;
            }
            
            .slicknav_nav li {
              border-bottom: 1px solid #ddd;
            }
            
            .slicknav_nav li.hide-desktop {
              border-bottom: 0px solid #ddd;
            }
            
            .slicknav_nav li:last-child {
              border-bottom: 0px solid #ddd;
            }
            
            .slicknav_nav {
              background: #fff none repeat scroll 0 0;
              border-top: 1px solid #198955;
              box-shadow: 0 0 25px rgba(0, 0, 0, 0.19);
            }
            
            .slicknav_btn {
              background-color: transparent !important;
              line-height: 0;
              margin-right: 0px;
              margin-top: -18px;
              float: left;
            }
            
            .slicknav_menu .slicknav_icon {
              float: none !important;
              margin: 0;
            }
            
            .slicknav_icon-bar {
              height: 3px !important;
              margin: 5px 0;
              width: 30px !important;
            }
            
            .slicknav_menu .slicknav_icon-bar {
              background-color: #fff;
            }
            
            .gauto-header-area.stick-top.sticky .slicknav_menu .slicknav_icon-bar,
            .gauto-header-area.home-3-page .slicknav_menu .slicknav_icon-bar {
              background-color: #fff;
            }
            
            
            
            /*================================================
            31 - NOTFOUND PAGE CSS
            ==================================================*/
            
            .notfound-box {
              width: 50%;
              margin: 0 auto;
              text-align: center;
            }
            
            .notfound-box h2 {
              font-size: 130px;
              color: #001238;
              letter-spacing: 1px;
              margin-bottom: 10px;
              -webkit-transition: all 0.4s ease 0s;
              transition: all 0.4s ease 0s;
              display: inline-block;
              text-transform: capitalize;
              font-family: "Rubik", sans-serif;
              font-weight: 600;
              line-height: 130px;
            }
            
            .notfound-box h3 {
              font-size: 30px;
              color: #001238;
              letter-spacing: 1px;
              margin-bottom: 10px;
              -webkit-transition: all 0.4s ease 0s;
              transition: all 0.4s ease 0s;
              display: block;
              text-transform: capitalize;
              font-family: "Rubik", sans-serif;
              font-weight: 600;
              line-height: 40px;
            }
            
            .notfound-box a.gauto-btn {
              margin: 30px 0 0 0;
              color: #198955;
            }
            
            .notfound-box a.gauto-btn:hover {
              color: #fff;
            }
            










            /*================ Normal desktop :992px.==================== */

@media (min-width: 992px) and (max-width: 1169px) {
  .header-top-left p {
    letter-spacing: 0;
  }
  .header-top-left p i {
    height: 20px;
    width: 20px;
    line-height: 20px;
  }
  .header-promo-info h3 {
    font-size: 17px;
  }
  .find-box {
    background: #fff url(/static/img/198955.png) no-repeat scroll -45px 0px !important;
  }
  .single-service {
    padding: 25px;
  }
  .service-text h3 {
    font-size: 19px;
  }
  .promo-box-right h3 {
    font-size: 30px;
  }
  .call-box-inner {
    width: 80%;
  }
  .product-text h3 {
    font-size: 17px;
    letter-spacing: 0;
  }
  .login-box {
    width: 55% !important;
  }
  .brochures a i {
    display: none;
  }
}

/*================ Tablet desktop :768px.==================*/

@media (min-width: 768px) and (max-width: 991px) {
  .ul-featured li {
    width: 50% !important;
  }
  .header-top-left:before {
    right: 0;
  }
  .gauto-responsive-menu {
    display: block;
    width: 50%;
    left: 0;
  }
  .header-action {
    display: none;
  }
  .mainmenu {
    display: none;
  }
  .search-box {
    display: none;
  }
  .main-search-right {
    -webkit-box-pack: end;
    -ms-flex-pack: end;
    justify-content: end;
    margin-top: 0;
  }
  .header-cart-box #dropdownMenu1 {
    position: relative;
    z-index: 999;
  }
  .gauto-about-area {
    padding-bottom: 20px;
  }
  .gauto-mainmenu-area {
    padding: 15px 0;
    min-height: 60px;
  }
  .header-cart-box {
    position: absolute;
    right: 20px;
    top: 0;
  }
  .slicknav_btn {
    margin-top: -27px;
  }
  .slider-text h2 {
    font-size: 50px;
  }
  .gauto-main-slide {
    height: 460px;
  }
  .find-box {
    background: #fff none repeat scroll 0 0 !important;
  }
  .find-text {
    margin-top: 25px;
  }
  .find-text h3 {
    font-size: 25px;
    color: #001238 !important;
    text-align: center !important;
  }
  .find-form form p {
    margin: 10px 0;
  }
  .about-left h2 {
    font-size: 35px;
  }
  .site-heading {
    width: 100%;
  }
  .site-heading h2 {
    font-size: 35px;
  }
  .promo-box-right {
    padding: 50px 0;
  }
  .promo-box-right h3 {
    font-size: 25px;
    line-height: 35px;
  }
  .offer-tabs .row .col-lg-4:last-child {
    display: none;
  }
  .gauto-blog-area .row .col-lg-4:last-child {
    display: none;
  }
  .offer-tabs .row .col-lg-4,
  .gauto-blog-area .row .col-lg-4 {
    width: 50%;
  }
  .single-footer {
    margin: 20px auto 20px;
  }
  .about-page-left h3,
  .about-promo-text h3 {
    font-size: 30px;
  }
  .about-page-right {
    margin-top: 30px;
  }
  .about-page-right img {
    width: 100%;
  }
  .about-promo-text {
    width: 80%;
  }
  .single-service {
    margin: 30px 0 0 0;
    border: 0px solid #fff;
    padding: 30px 15px;
  }
  .service-text h3 {
    font-size: 17px;
    letter-spacing: 0;
  }
  .service-icon {
    width: 50px;
  }
  .service-menu {
    padding: 20px;
  }
  .service-page-banner {
    padding: 50px 30px;
  }
  .service-page-banner h3 {
    font-size: 22px !important;
    line-height: 32px !important;
  }
  .service-details-right {
    margin-top: 30px;
  }
  .faq_accordian_header > a.collapsed:before {
    width: 14%;
  }
  .product-details-text {
    margin-top: 30px;
  }
  .faq_accordian_header > a:before {
    width: 14%;
  }
  .car-listing-right {
    margin-top: 30px;
  }
  .paging_status {
    -webkit-box-pack: end;
    -ms-flex-pack: end;
    justify-content: end;
  }
  .propertu-page-shortby {
    display: none;
  }
  .offer-action:after {
    width: 62%;
  }
  .car-booking-image img {
    width: 100%;
  }
  .car-booking-right {
    margin-top: 30px;
  }
  .booking-right {
    margin-top: 30px;
  }
  .single-gallery {
    width: 80%;
    margin: 30px auto 0;
  }
  .product-page-right {
    margin-top: 30px;
  }
  .order-summury-box {
    margin-top: 30px;
  }
  .blog-page-right {
    margin-top: 30px;
  }
  .notfound-box {
    width: 80%;
  }
  .notfound-box h2 {
    font-size: 90px;
    line-height: 90px;
  }
  .notfound-box h3 {
    font-size: 26px;
  }
  .login-box {
    width: 70% !important;
  }
  .contact-right {
    margin-top: 30px;
  }
  .newsletter_box form {
    width: 100%;
  }
  .site-logo {
    margin-top: 5px;
  }
}

/*================== small mobile :320px. ===================*/

@media (max-width: 767px) {
  .feature-grid-view {
    top: 0px !important;
  }
  .ul-featured li {
    width: 50% !important;
  }
  .header-top-left:before {
    display: none;
  }
  .header-top-left {
    text-align: center !important;
  }
  .header-top-right {
    text-align: center !important;
  }
  .gauto-responsive-menu {
    display: block;
  }
  .site-logo {
    text-align: center;
    width: 55%;
    margin: 0 auto;
  }
  .site-logo img {
    width: 100%;
  }
  .gauto-about-area {
    padding-bottom: 50px;
  }
  .header-promo,
  .header-action {
    display: none;
  }
  .gauto-header-top-area {
    margin-bottom: 0;
  }
  .mainmenu {
    display: none;
  }
  .gauto-header-top-area {
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #eee;
  }
  .brochures a i {
    display: none;
  }
  .header-top-left p,
  .header-top-right > a,
  .header-top-right > .dropdown button {
    color: #001238;
  }
  .header-top-left p i {
    background: #198955 none repeat scroll 0 0;
    color: #fff;
  }
  .search-box {
    display: none;
  }
  .header-cart-box #dropdownMenu1 {
    float: right;
  }
  .service-text h3 {
    font-size: 19px;
  }
  .header-cart-box {
    width: 95%;
  }
  .login.dropdown {
    position: relative;
    z-index: 999999;
    width: 100%;
  }
  .gauto-mainmenu-area {
    min-height: 50px;
  }
  .header-cart-box .dropdown.show .cart-dropdown {
    margin-top: 35px;
    min-width: 260px;
  }
  .cart-btn-pro-img {
    width: 60px;
    margin-right: 10px;
  }
  .cart-btn-pro-cont {
    overflow: hidden;
  }
  .slider-text {
    width: 100%;
    text-align: center;
    margin: 0 auto;
  }
  .slider-text p {
    font-size: 20px;
  }
  .slider-text h2 {
    font-size: 35px;
  }
  .gauto-main-slide {
    height: 380px;
  }
  .find-box {
    background: #fff none repeat scroll 0 0 !important;
  }
  .find-text {
    margin-top: 0;
  }
  .find-text h3 {
    color: #001238 !important;
    text-align: center;
    font-size: 25px !important;
  }
  .find-form {
    margin-top: 5px;
  }
  .find-form form p {
    margin-top: 15px;
    margin-bottom: 0;
  }
  .gauto-find-area {
    margin-top: -55px;
  }
  .about-left h2 {
    font-size: 30px;
  }
  .about-list ul {
    -webkit-columns: 1;
    -moz-columns: 1;
    columns: 1;
  }
  .signature-left {
    width: 130px;
  }
  .site-heading {
    width: 100%;
  }
  .site-heading h2 {
    font-size: 30px;
  }
  .promo-box-left {
    display: none;
  }
  .promo-box-right {
    text-align: center;
  }
  .promo-box-right h3 {
    font-size: 28px;
    line-height: 40px;
  }
  .promo-box-right a.gauto-btn {
    margin-bottom: 0;
  }
  #offerTab li {
    display: block;
    margin-bottom: 5px;
  }
  #offerTab li a {
    display: block !important;
  }
  #offerTab li.nav-item a.nav-link.active:after,
  #offerTab li.nav-item a.nav-link:after {
    -webkit-transform: skewX(0);
    transform: skewX(0);
  }
  #offerTab li.nav-item a.nav-link:hover:after {
    -webkit-transform: skewX(0);
    transform: skewX(0);
  }
  .call-box-inner {
    width: 100%;
    padding: 0 15px;
  }
  .call-box h2 {
    font-size: 28px;
    line-height: 40px;
  }
  .single-footer {
    margin: 20px 0;
  }
  .copyright {
    text-align: center;
    margin-bottom: 10px;
  }
  .footer-social ul {
    text-align: center;
  }
  .breadcromb-box h3 {
    font-size: 30px;
  }
  .about-page-left h3,
  .about-promo-text h3 {
    font-size: 28px;
    line-height: 40px;
  }
  .about-page-right {
    margin-top: 30px;
  }
  .about-promo-text {
    width: 100%;
  }
  .service-details-right {
    margin-top: 30px;
  }
  .service-details-right h3,
  .sidebar-widget h3 {
    font-size: 22px;
    line-height: 40px;
  }
  .pad-right-sm {
    padding-right: 15px;
  }
  .pad-left-sm {
    padding-left: 15px;
  }
  .service-details-list ul {
    width: 100%;
    float: none;
    margin: 0;
  }
  .service-details-list.clearfix {
    margin-top: 30px;
  }
  .faq_accordian_header > a.collapsed:before {
    width: 30%;
  }
  .faq_accordian_header > a:before {
    width: 30%;
  }
  .car-listing-right {
    margin-top: 30px;
  }
  .propertu-page-shortby {
    display: none;
  }
  .car-booking-right {
    margin-top: 30px;
  }
  .car-booking-right h3,
  .product-details-text h3 {
    font-size: 28px;
    line-height: 40px;
  }
  .car-features ul {
    width: 50% !important;
  }
  .booking-right {
    margin-top: 30px;
  }
  .product-page-right,
  .product-details-text {
    margin-top: 30px;
  }
  .product-text h3 {
    font-size: 21px;
  }
  .single-shop-page-btn {
    display: block;
  }
  .single-shop-page-btn ul {
    margin-left: 0;
    margin-top: 20px;
  }
  .order-summury-box {
    margin-top: 30px;
  }
  .checkout-action {
    text-align: left;
  }
  .blog-text {
    padding: 15px 10px;
  }
  .blog-text ul li {
    margin-right: 5px;
  }
  .blog-page-left .blog-text h3 {
    font-size: 22px;
    line-height: 36px;
  }
  .blog-page-right {
    margin-top: 30px;
  }
  .single-offers {
    padding: 10px;
  }
  .offer-text ul li {
    margin: 0 1px;
  }
  .offer-text ul li i {
    margin-right: 1px;
  }
  .single-comment-box.reply-comment {
    padding-left: 30px;
  }
  .gauto-leave-comment form input {
    width: 100%;
    margin-right: 0;
  }
  .gauto-leave-comment form textarea {
    width: 100%;
  }
  .notfound-box {
    width: 100%;
  }
  .notfound-box h2 {
    font-size: 70px;
    line-height: 70px;
  }
  .notfound-box h3 {
    font-size: 22px;
  }
  .login-box {
    width: 100% !important;
    padding: 30px 15px !important;
  }
  .contact-right {
    margin-top: 30px;
  }
  .newsletter_box form {
    width: 100%;
  }
  .cart-btn-pro-cont h4,
  .cart-btn-pro-cont h4 a {
    font-size: 15px;
  }
  .cart-btn-pro-cont span.price {
    font-size: 14px;
  }
}

/* ======================Large Mobile :480px.================== */

@media only screen and (min-width: 480px) and (max-width: 767px) {
  .ul-featured li {
    width: 50% !important;
  }
  .header-top-left:before {
    display: none;
  }
  .header-top-left {
    text-align: center !important;
  }
  .header-top-right {
    text-align: center !important;
  }
  .gauto-responsive-menu {
    display: block;
  }
  .site-logo {
    text-align: center;
    width: 45%;
    margin: 0 auto;
  }
  .gauto-about-area {
    padding-bottom: 50px;
  }
  .site-logo img {
    width: 100%;
  }
  .single-service {
    padding: 20px;
  }
  .service-text h3 {
    font-size: 18px;
    letter-spacing: 0;
  }
  .header-promo,
  .header-action {
    display: none;
  }
  .gauto-header-top-area {
    margin-bottom: 0;
  }
  .mainmenu {
    display: none;
  }
  .gauto-header-top-area {
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #eee;
  }
  .header-top-left p,
  .header-top-right > a,
  .header-top-right > .dropdown button {
    color: #001238;
  }
  .header-top-left p i {
    background: #198955 none repeat scroll 0 0;
    color: #fff;
  }
  .offer-action:after {
    width: 62%;
  }
  .service-details-image img {
    width: 100%;
  }
  .search-box {
    display: none;
  }
  .header-cart-box #dropdownMenu1 {
    float: right;
  }
  .header-cart-box {
    width: 95%;
  }
  .login.dropdown {
    position: relative;
    z-index: 999999;
    width: 100%;
  }
  .gauto-mainmenu-area {
    min-height: 50px;
  }
  .header-cart-box .dropdown.show .cart-dropdown {
    margin-top: 35px;
    min-width: 260px;
  }
  .cart-btn-pro-img {
    width: 60px;
    margin-right: 10px;
  }
  .cart-btn-pro-cont {
    overflow: hidden;
  }
  .slider-text {
    width: 100%;
    text-align: center;
    margin: 0 auto;
  }
  .slider-text p {
    font-size: 20px;
  }
  .slider-text h2 {
    font-size: 35px;
  }
  .gauto-main-slide {
    height: 380px;
  }
  .find-box {
    background: #fff none repeat scroll 0 0 !important;
  }
  .find-text {
    margin-top: 0;
  }
  .find-text h3 {
    color: #001238 !important;
    text-align: center !important;
    font-size: 25px !important;
  }
  .single-offers {
    padding: 15px;
  }
  .offer-text ul li {
    margin: 0 3px;
  }
  .offer-text ul li i {
    margin-right: 3px;
  }
  .find-form {
    margin-top: 5px;
  }
  .find-form form p {
    margin-top: 15px;
    margin-bottom: 0;
  }
  .gauto-find-area {
    margin-top: -55px;
  }
  .about-left h2 {
    font-size: 30px;
  }
  .about-list ul {
    -webkit-columns: 1;
    -moz-columns: 1;
    columns: 1;
  }
  .signature-left {
    width: 130px;
  }
  .site-heading {
    width: 100%;
  }
  .site-heading h2 {
    font-size: 30px;
  }
  .promo-box-left {
    display: none;
  }
  .promo-box-right {
    text-align: center;
  }
  .promo-box-right h3 {
    font-size: 28px;
    line-height: 40px;
  }
  .promo-box-right a.gauto-btn {
    margin-bottom: 0;
  }
  #offerTab li {
    display: block;
    margin-bottom: 5px;
  }
  #offerTab li a {
    display: block !important;
  }
  #offerTab li.nav-item a.nav-link.active:after,
  #offerTab li.nav-item a.nav-link:after {
    -webkit-transform: skewX(0);
    transform: skewX(0);
  }
  #offerTab li.nav-item a.nav-link:hover:after {
    -webkit-transform: skewX(0);
    transform: skewX(0);
  }
  .call-box-inner {
    width: 100%;
    padding: 0 15px;
  }
  .service-text h3 {
    font-size: 18px;
  }
  .call-box h2 {
    font-size: 28px;
    line-height: 40px;
  }
  .single-footer {
    margin: 20px 0;
  }
  .copyright {
    text-align: center;
    margin-bottom: 10px;
  }
  .footer-social ul {
    text-align: center;
  }
  .breadcromb-box h3 {
    font-size: 30px;
  }
  .about-page-left h3,
  .about-promo-text h3 {
    font-size: 28px;
    line-height: 40px;
  }
  .about-page-right {
    margin-top: 30px;
  }
  .about-promo-text {
    width: 100%;
  }
  .service-details-right {
    margin-top: 30px;
  }
  .service-details-right h3,
  .sidebar-widget h3 {
    font-size: 22px;
    line-height: 40px;
  }
  .pad-right-sm {
    padding-right: 15px;
  }
  .pad-left-sm {
    padding-left: 15px;
  }
  .service-details-list ul {
    width: 100%;
    float: none;
    margin: 0;
  }
  .service-details-list.clearfix {
    margin-top: 30px;
  }
  .faq_accordian_header > a.collapsed:before {
    width: 20%;
  }
  .faq_accordian_header > a:before {
    width: 20%;
  }
  .car-listing-right {
    margin-top: 30px;
  }
  .propertu-page-shortby {
    display: none;
  }
  .car-booking-right {
    margin-top: 30px;
  }
  .car-booking-right h3,
  .product-details-text h3 {
    font-size: 28px;
    line-height: 40px;
  }
  .car-features ul {
    width: 33% !important;
  }
  .booking-right {
    margin-top: 30px;
  }
  .product-page-right,
  .product-details-text {
    margin-top: 30px;
  }
  .product-text h3 {
    font-size: 21px;
  }
  .single-shop-page-btn {
    display: block;
  }
  .single-shop-page-btn ul {
    margin-left: 0;
    margin-top: 20px;
  }
  .order-summury-box {
    margin-top: 30px;
  }
  .checkout-action {
    text-align: left;
  }
  .blog-text {
    padding: 15px 10px;
  }
  .blog-text ul li {
    margin-right: 5px;
  }
  .blog-page-left .blog-text h3 {
    font-size: 22px;
    line-height: 36px;
  }
  .blog-page-right {
    margin-top: 30px;
  }
  .single-comment-box.reply-comment {
    padding-left: 30px;
  }
  .gauto-leave-comment form input {
    width: 100%;
    margin-right: 0;
  }
  .gauto-leave-comment form textarea {
    width: 100%;
  }
  .notfound-box {
    width: 100%;
  }
  .notfound-box h2 {
    font-size: 70px;
    line-height: 70px;
  }
  .notfound-box h3 {
    font-size: 22px;
  }
  .single-testimonial {
    padding: 20px;
  }
  .client-info h3 {
    font-size: 18px;
  }
  .login-box {
    width: 100% !important;
    padding: 30px 15px !important;
  }
  .contact-right {
    margin-top: 30px;
  }
  .newsletter_box form {
    width: 100%;
  }
  .cart-btn-pro-cont h4,
  .cart-btn-pro-cont h4 a {
    font-size: 15px;
  }
  .cart-btn-pro-cont span.price {
    font-size: 14px;
  }
}

          `}
        </style>
      </html>
    );
  }
}

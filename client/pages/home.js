// This JS file is for /home page.

// Packages Imports
import React from "react";
import HomePageContainer from "../src/containers/HomePageContainer";

// Home Component Class
class HomePage extends React.Component {
  // Home Page Constructor
  constructor(props) {
    super(props);
  }

  // Home Page Render function
  render() {
    // Returning Home Page Container here - Starting Point
    return <HomePageContainer />
  }
}

export default HomePage;
// Register Page JS file

// Imports 
import React from "react";
import Head from "next/head";
import RegisterContainer from "containers/RegisterContainer"; // Register Container

// Register Page Component
class RegisterPage extends React.Component {
  
  // Register Page Constructor
  constructor(props) {
    super(props);
  }

  // Render method
  render() {
    return (
      <div>
        {/* Title added in Head */}
        <Head>
          <title>Quiz System</title>
        </Head>

        {/* Register Container */}
        <RegisterContainer />
      </div>
    );
  }
}

export default RegisterPage;

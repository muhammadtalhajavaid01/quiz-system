// This JS file is for /social page.

// Packages Imports
import React from "react";
import SocialContainer from "../src/containers/SocialContainer";

// React Component Class 
class SocialPage extends React.Component {
  
  // Social Page Constructor
  constructor(props) {
    super(props);
  }

  // Social Page Render function
  render() {
    // Returning Social Page Container here - Starting Point
    return <SocialContainer />
  }
}

export default SocialPage;
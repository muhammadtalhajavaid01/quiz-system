// Login Page JS

// imports 
import React from "react";
import Head from "next/head";
import LoginContainer from "containers/LoginContainer"; // Login Container


// Login Page Component
class LoginPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        {/* Adding Title in Head */}
        <Head>
          <title>Quiz System</title>
        </Head>

        {/* Login Container Tag */}
        <LoginContainer />
      </div>
    );
  }
}

export default LoginPage;
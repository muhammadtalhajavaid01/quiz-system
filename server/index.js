const app = require('./server/app');

// Start the server - Port number
const port = 3600;

app.listen(port);
console.log(`Server listening at ${port}`);


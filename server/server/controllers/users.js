// Imports
const Users = require("../models/users"); // Users Model Required
const counter = require("../helpers/counter"); // Counter for Auto Increament PK in DB
const bcrypt = require("bcryptjs"); // For Password Hashing.

module.exports = {
  // User login functionality
  login: async (req, res) => {
    // Retrieving Request params
    const { userName, password } = req.body;

    // Chcking is user exists already
    const user = await Users.findOne({ userName: userName });

    // If not, handle it
    if (!user) {
      res
        .status(200)
        .json({
          errorCode: "E_101",
          status: false,
          message: "You have provided invalid credentials"
        });
    }

    // Check if the password is correct
    const isMatch = await user.isValidPassword(password);

    // If not, handle it
    if (!isMatch) {
      res
        .status(200)
        .json({
          errorCode: "E_101",
          status: false,
          message: "You have provided invalid credentials"
        });
    }

    // Success response
    res
      .status(200)
      .json({
        status: true,
        message: "Successfully LoggedIn",
        userName: user.userName
      });
  },

  // User Signup functionality
  signUp: async (req, res) => {
    try {
      // Retrieving Request params
      const { userName, email, password } = req.body;

      // Check if there is a user with the same email or username already
      const foundUser = await Users.findOne({
        $or: [{ email: email }, { userName: userName }]
      });
      if (foundUser) {
        return res
          .status(200)
          .json({
            status: false,
            message: "This Email Address Or User Name is already in use"
          });
      }

      // User PK auto increment
      const userId = await counter.incrementId("user");

      // Encrypt Password
      const salt = await bcrypt.genSalt(10);
      const passwordHash = await bcrypt.hash(password, salt);

      // Creating user obj
      const newUser = new Users({
        id: userId,
        userName: userName,
        email: email,
        password: passwordHash,
        status: true
      });

      // User creation
      await newUser.save();

      // Return success response
      res
        .status(200)
        .json({
          status: true,
          message: "Your account has been registered successfully"
        });
    } catch (error) {
      // Managing try-catch
      res.status(200).json({ status: false, message: error.message });
    }
  }
};

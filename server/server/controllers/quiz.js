// Imports
const Quiz = require("../models/quiz");
const counter = require("../helpers/counter");

// Exporting module functionalities
module.exports = {

  // Add Quiz Marks functionality
  add: async (req, res) => {
    try {
      // This is for incrementing id in quiz table in database for new entry
      const quizId = await counter.incrementId("quiz");
      const userQuiz = await Quiz.findOne({ "userName": req.body.userName });

      if (userQuiz) {
        userQuiz.marks = req.body.marks;
        userQuiz.save();
      } else {
        // Creating new Quiz object containing increamented id, userName and Marks
        const newQuiz = new Quiz({
          id: quizId,
          userName: req.body.userName,
          marks: req.body.marks
        });

        // Quiz Marks creation
        await newQuiz.save();
      }

      // Returning success response
      res.status(200).json({
        status: true,
        message: "Marks added Successfully"
      });
    } catch (error) {
      // Managing try - catch
      res.status(200).json({ status: false, message: error.message });
    }
  },

  // Get list of threads created
  get: async (req, res) => {
    try {

      // querying db to fetching Quiz marks for the user
      const userQuiz = await Quiz.find({ "userName": req.body.userName });

      // Return success response containing list of threads
      res.status(200).json({
        status: true,
        marks: userQuiz
      });
    } catch (error) {
       // Managing try - catch
      res.status(200).json({ status: false, marks: null });
    }
  }
};

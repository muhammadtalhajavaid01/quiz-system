// Imports
const Thread = require("../models/thread"); // Thread Model
const ThreadDetail = require("../models/threadDetail"); // Thread Replies
const counter = require("../helpers/counter"); // Autoincrement PK in DB

// Exporting module functionalities
module.exports = {
  // Start new thread functionality
  add: async (req, res) => {
    try {
      // Text got from http request ... Question of new thread
      const { text } = req.body;

      // This is for incrementing id in thread table in database for new entry
      const threadId = await counter.incrementId("thread");

      // Creating new Thread object containing increamented id and question for new thread
      const newThread = new Thread({
        id: threadId,
        question: text
      });

      // Thread creation
      await newThread.save();

      // Returning success response
      res.status(200).json({
        status: true,
        message: "Thread has been created successfully"
      });
    } catch (error) {
      // Managing try - catch
      res.status(200).json({ status: false, message: error.message });
    }
  },

  // Get list of threads created
  get: async (req, res) => {
    try {

      // querying db to fetching all threads
      const threads = await Thread.find();

      // Return success response containing list of threads
      res.status(200).json({
        status: true,
        threads: threads
      });
    } catch (error) {
       // Managing try - catch
      res.status(200).json({ status: false, threads: [] });
    }
  },

  // Add Reply to the existing thread
  addreply: async (req, res) => {
    try {

      // Incrementing thread reply number for DB
      const threadDetailId = await counter.incrementId("threadDetail");

      const currentdate = new Date();

      // Creating new object for threadDetail table containing thread detail id, question id (thread id), reply text
      const newThreadDetailObj = new ThreadDetail({
        id: threadDetailId,
        threadId: req.body.id,
        reply: req.body.reply,
        userName: req.body.userName,
        createdDate: currentdate.getDate() + "/"+ (currentdate.getMonth()+1)  + "/" + currentdate.getFullYear() + " " +  currentdate.getHours() + ":"  + currentdate.getMinutes() + ":" + currentdate.getSeconds()
      });

      // creating reply in db
      await newThreadDetailObj.save();

      // Returning success response to the user.
      res.status(200).json({
        status: true
      });
    } catch (error) {
      // Managing try - catch here
      res.status(200).json({ status: false });
    }
  },

  // Get all Replies of a specific thread.
  getreplies: async (req, res) => {
    try {

      // get list of replies
      let threadDetailList = await ThreadDetail.find({
        threadId: req.body.id
      });

      // If no, put empty array
      if (!threadDetailList) {
        threadDetailList = [];
      }

      // Return success response.
      res.status(200).json({
        status: true,
        list: threadDetailList
      });
    } catch (error) {
      res.status(200).json({ status: false, list: [] });
    }
  }
};

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create a schema
const quizSchema = new Schema({
  id: {
    type: Number
  },
  userName: {
    type: String
  },
  marks: {
    type: Number
  }
});


// Create a model
const quiz = mongoose.model('quiz', quizSchema);

// Export the model
module.exports = quiz;

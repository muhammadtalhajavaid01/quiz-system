const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create a schema
const threadDetailSchema = new Schema({
  id: {
    type: Number
  },
  threadId: {
    type: Number
  },
  reply: {
    type: String
  },
  userName: {
    type: String
  },
  createdDate: {
    type: String
  }
});

// Create a model
const threadDetail = mongoose.model("threadDetail", threadDetailSchema);

// Export the model
module.exports = threadDetail;

const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

// Create a schema
const usersSchema = new Schema({
  id: {
    type: Number
  },
  userName: {
    type: String
  },
  email: {
    type: String,
  },
  password: {
    type: String
  },
  status: {
    type: Boolean
  }
});

usersSchema.methods.isValidPassword = async function(newPassword) {
  try {
    return await bcrypt.compare(newPassword, this.password);
  } catch(error) {
    throw new Error(error);
  }
}

// Create a model
const users = mongoose.model('users', usersSchema);

// Export the model
module.exports = users;

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create a schema
const threadSchema = new Schema({
  id: {
    type: Number
  },
  question: {
    type: String
  }
});


// Create a model
const thread = mongoose.model('thread', threadSchema);

// Export the model
module.exports = thread;

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./configuration/index');

mongoose.Promise = global.Promise;


// Connecting with mongoDB
config.DB = mongoose.connect('mongodb://localhost/quiz-system', { useMongoClient: true });

const app = express();

// If there are cookies, parse them
var cookieParser = require('cookie-parser');
app.use(cookieParser());

// CORS enabling to allow access server from any origin
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
  res.header('Access-Control-Expose-Headers', 'Content-Length');
  res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');

  if (req.method === 'OPTIONS') {
      return res.sendStatus(200);
  } else {
      return next();
  }
});

// Parsing http request
app.use(bodyParser.json());

// Routes
app.use('/users', require('./routes/users'));
app.use('/thread', require('./routes/community'));
app.use('/quiz', require('./routes/quiz'));
module.exports = app;

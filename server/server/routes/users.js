// Routing from express-promise
const router = require('express-promise-router')();

// User Controller
const UsersController = require('../controllers/users');

// Sign up route - API
router.route('/signup')
  .post(UsersController.signUp);

// Login Route - API
router.route('/login')
  .post(UsersController.login);

module.exports = router;

// Routing through express promise
const router = require('express-promise-router')();

// Community - Threads controller
const CommunityController = require('../controllers/community');

// Route to start new thread functionality
router.route('/add')
  .post(CommunityController.add);

// Route to get threads functionality
router.route('/get')
  .post(CommunityController.get);

// Route to add reply functionality
router.route('/addreply')
  .post(CommunityController.addreply);

// Route to get thread replies functionality
router.route('/getreplies')
  .post(CommunityController.getreplies);

module.exports = router;

// Routing through express promise
const router = require('express-promise-router')();

// Quiz Controller
const QuizController = require('../controllers/quiz');

// Route to add quiz marks functionality
router.route('/add')
  .post(QuizController.add);

// Route to get quiz marks functionality
router.route('/get')
  .post(QuizController.get);

module.exports = router;

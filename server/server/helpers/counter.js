const config = require('../configuration/index');

module.exports = {
  // This method is to do increments in PK for DB
  incrementId: async (type) => {

    return new Promise((resolve,reject)=>{
      config.DB.collection('counter').findAndModify(
        {id: type},
        [['_id','asc']],
        { "$inc": { "seq":1 } },
        {new: true, upsert: true},
        async function(err, doc){
          resolve(doc.value.seq);
        });
    });
  }
}
